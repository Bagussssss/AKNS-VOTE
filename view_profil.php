<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		h3 {font-family: georgia;}
		b {font-family: georgia; font-size: 17px;}
		ol li {font-family: georgia; font-style: italic;}

	</style>
</head>
<body>

	<div class="row wadah efek">
		<div class="col-lg-12">

			<div class="row">
				<div class="col-lg-12">
					<h2><i class="fas fa-poll-h"></i> Profil Calon</h2>
					<p>Anda Bisa melihat Profil dan mengenal Visi & Misi Calon di halaman ini.</p>
					<hr style="margin-bottom: 5px;">
				</div>
			</div>

			<div class="row" style="margin-top: 10px;">
				<div class="col-md-12" id="wadahnya">

					<div class="row">

						<div class="col-md-12">
							<?php 
							include 'koneksi.php';
								$id = $_GET['nomer_calon'];

								$query = mysqli_query($conn, "SELECT * FROM capres where NOMER_URUT = '$id'");
								$d = mysqli_fetch_array($query); ?>
								<div class="row thumbnail" style="padding-top: 20px; margin-left: 1px; width: 99.9%; background: whitesmoke;">
									<div class="col-md-6">
										<!-- <h3 style="margin-top: 0px; text-align: center;">Profil Calon Presiden & Wakil Presiden Nomer Urut</h3> -->

										<center><b>Visi</b>
											<p><?php echo $d['VISI']; ?></p>
											<b>Misi</b></center>
											<p class="misi"><?php echo $d['MISI']; ?></p>
											<hr>
										</div>

										<div class="col-md-6">
											<div class="thumbnail fotoview">
												<img src="assets/img/<?php echo $d['FOTOCALON'] ?>" style="height: 300px;">
												<div class="caption text-center">
													<h4 style="margin-bottom: 3px;">PASLON 0<?= $d['NOMER_URUT'] ?></h4>
													<p style="margin-top: 0px;"><?= $d['NAMA_CAPRES'] ?> - <?= $d['NAMA_CAWAPRES'] ?></p>
													<p style="font-size: 10px;">"<?php echo $d['SLOGAN']; ?>"</p>
													</div>
												</div>
											</div>

											<div class="col-md-6">
												<b>Program Kerja :</b>
												<p>
												<?php echo $d['program_kerja']; ?>
												</p>
											</div>
											<div class="col-md-6">
												<p>Profil Capres :</p>
												<table class="table">
													<tr>
														<td>Nama Lengkap</td>
														<td>:</td>
														<td><?php echo $d['NAMA_CAPRES']; ?></td>
													</tr>
													<tr>
														<td>Tanggal Lahir</td>
														<td>:</td>
														<td><?php echo $d['PROFIL_CAPRES']; ?></td>
													</tr>
													<tr>
														<td>Pekerjaan</td>
														<td>:</td>
														<td><?php echo $d['PEKERJAAN_CAPRES']; ?></td>
													</tr>
													<tr>
														<td>Jurusan</td>
														<td>:</td>
														<td><?php echo $d['JURUSAN_CAPRES']; ?></td>
													</tr>
												</table>
												<p>Profil Cawapres :</p>
												<table class="table">
													<tr>
														<td>Nama Lengkap</td>
														<td>:</td>
														<td><?php echo $d['NAMA_CAWAPRES']; ?></td>
													</tr>
													<tr>
														<td>Tanggal Lahir</td>
														<td>:</td>
														<td><?php echo $d['PROFIL_CAWAPRES']; ?></td>
													</tr>
													<tr>
														<td>Pekerjaan</td>
														<td>:</td>
														<td><?php echo $d['PEKERJAAN_CAWAPRES']; ?></td>
													</tr>
													<tr>
														<td>Jurusan</td>
														<td>:</td>
														<td><?php echo $d['JUR_CAWAPRES']; ?></td>
													</tr>
												</table>
											</div>
										</div>

										<div class="row">
											<div class="col-md-12 text-right">
												<p>© <?php echo $tahun = gmdate('Y'); ?> - <?php echo $tahun+1; ?></p>
											</div>
										</div>
								</div>

							</div>



						</div> <!-- akhir dari wadah -->
					</div>





				</div> <!-- akhir col-lg-12 -->



			</body>
			</html>