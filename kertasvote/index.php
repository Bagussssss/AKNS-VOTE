<?php 
session_start();
	if (!isset($_SESSION['NRP'])) {
		?>
			<script type="text/javascript">
				window.location.href="../index.php?k=home";
			</script>
		<?php 
	} else {
?>
<!DOCTYPE html>
<html>
<head>
	<link rel="shourcut icon" href="../assets/img/photo.jpg">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>RUANG VOTING | BEM AKNS</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../assets/fontku/css/all.css">
	<script src="../assets/alert/sweetalert2.min.js"></script>
	<link rel="stylesheet" href="../assets/alert/sweetalert2.min.css">
	<style type="text/css">
	@font-face {
		font-family: "blending-italic";
		src : url('../assets/fonts/GoogleSans-Regular.ttf');
	}
	*{font-family:blending-italic;}
		.judul h3 {
			font-size: 22px;
		}
		[type="radio"]:checked,
		[type="radio"]:not(:checked) {
		    position: absolute;
		    left: -9999px;
		}
		[type="radio"]:checked + label,
		[type="radio"]:not(:checked) + label
		{
		    position: relative;
		    padding-left: 28px;
		    cursor: pointer;
		    line-height: 20px;
		    display: inline-block;
		    color: #666;
		}
		[type="radio"]:checked + label:before,
		[type="radio"]:not(:checked) + label:before {
		    content: '';
		    position: absolute;
		    left: 0;
		    top: 0;
		    width: 30px;
		    height: 30px;
		    border: 1px solid black;
		    border-radius: 100%;
		    background: #fff;
		}
		[type="radio"]:checked + label:after,
		[type="radio"]:not(:checked) + label:after {
		    content: '';
		    width: 22px;
		    height: 22px;
		    background: blue;
		    position: absolute;
		    top: 4px;
		    left: 4px;
		    border-radius: 100%;
		    -webkit-transition: all 0.2s ease;
		    transition: all 0.2s ease;
		}
		[type="radio"]:not(:checked) + label:after {
		    opacity: 0;
		    -webkit-transform: scale(0);
		    transform: scale(0);
		}
		[type="radio"]:checked + label:after {
		    opacity: 1;
		    -webkit-transform: scale(1);
		    transform: scale(1);
		}
		
    	#loader {

			position: absolute;

			left: 50%;

			top: 50%;

			z-index: 1;

			width: 100px;

			height: 100px;

			margin: -75px 0 0 -75px;

			border: 16px solid #f3f3f3;

			border-radius: 50%;

			border-top: 16px solid #3498db;

			width: 120px;

			height: 120px;

			-webkit-animation: spin 2s linear infinite;

			animation: spin 2s linear infinite;

			}



			@-webkit-keyframes spin {

			0% { -webkit-transform: rotate(0deg); }

			100% { -webkit-transform: rotate(360deg); }

			}



			@keyframes spin {

			0% { transform: rotate(0deg); }

			100% { transform: rotate(360deg); }

			}



			/* Add animation to "page content" */

			.animate-bottom {

			position: relative;

			-webkit-animation-name: animatebottom;

			-webkit-animation-duration: 1s;

			animation-name: animatebottom;

			animation-duration: 1s

			}



			@-webkit-keyframes animatebottom {

			from { bottom:-100px; opacity:0 }

			to { bottom:0px; opacity:1 }

			}



			@keyframes animatebottom {

			from{ bottom:-100px; opacity:0 }

			to{ bottom:0; opacity:1 }

			}



			#myDiv {

			display: none;

			}
 		a{text-decoration:none; color: black;}
		a .card:hover {box-shadow:5px 5px 1px rgb(0,0,0,0.10); transition:0.4s; color: black;}
		.card:hover .hiddenvote {display: block; transition: 0.7s;}
		a:hover{text-decoration:none;}
		.hiddenvote{display: none;}
		@media screen and (max-width: 750px){
			.card img {max-height: 200px;}
		}
	 </style>
</head>
<body onload="myFunction()">
<div id="loader"></div>
<div style="display:none;" id="myDiv" class="animate-bottom">
<div class="container">
<!-- 	<div class="row">
		<div class="col-md-12">
			<h3>Anda Login Sebagai <?php echo $_SESSION['NRP']; ?></h3>
		</div>
	</div> -->
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8" style="border: 1px solid #dfdfdf; margin-top: 10px; margin-bottom: 10px; border-radius: 5px;">

			<div class="bungkus" style="padding: 10px;">

				<div class="row">
					<div class="col-md-12 text-center">
						<img src="../assets/img/cop2.jpg" width="100%">
					</div>
				</div>

				<div class="row">
					<div class="col-md-12 text-center judul">
						<h3 style="margin-top: 2px; margin-bottom: 2px;">Pemilihan Presiden dan Wakil Presiden</h3>
						<h3 style="margin-top: 0px;">Tahun Bakti <?php echo gmdate("Y"); ?>
						- <?php $tgl= gmdate("Y"); echo $jml = ($tgl+1); ?></h3>
					</div>
				</div>

		<!-- proses form input untuk voting -->

			<form method="post" action="input.php">

				<div class="row" style="margin-top: 20px;">
					<div class="col-md-12">
						
						<div class="row justify-content-center kolomcoblos">

							<?php 
								include '../koneksi.php';
								$data = mysqli_query($conn, "SELECT * FROM CAPRES order by NOMER_URUT");

								while ($row = mysqli_fetch_array($data)) { ?>

									<div class="col-sm-5">
									<a href="input.php?NOMER_URUT=<?php echo $row['NOMER_URUT'] ?>" onclick="return confirm('Apakah Anda Yakin Dengan Pilihan Anda ?')">
										<div class="card" style="margin-bottom: 20px; height: 400px;">
									      <img src="../assets/img/<?= $row['FOTOCALON'] ?>" style="height: 200px;" width="100%">
									      <div class="caption text-center">
									        <h4 style="margin-bottom: 3px;">PASLON 0<?= $row['NOMER_URUT'] ?></h4>
									        <p style="margin-top: 0px; margin-bottom: 0px;">
									        	<?= $row['NAMA_CAPRES'] ?>
									        </p>
									        <p style="margin-top: 0px;">
									        	<?= $row['NAMA_CAWAPRES'] ?>
									        </p>
									        <span class="text-center">
									        	<i><p style="font-size: 10px;">
									        		<?= $row['SLOGAN'] ?>
									        	</p></i>
									        </span>

									        <div class="row">
									        	<div class="col-md-12 hiddenvote">
									        		<img src="../assets/vote/online-voting.png" width="70">
									        	</div>
									        </div>
									      </div>
									    </div>
										</a>
									</div>

									<?php
								}
							?>

							
						</div> <!-- akhir kolomcoblos -->

					</div>
				</div>

			</form>
		<!-- akhir form untuk voting -->

				<div class="row keterangan" style="margin-top: 40px;">
					<div class="col-md-1"></div>
					<div class="col-md-10 text-center">
						<p style="color: grey; font-style: italic; font-size: 13px;">
							*Klik Salah Satu Gambar Pasangan Calon Yang Tertera Diatas Untuk Menentukan Pilihan Anda. <br>
							(Login Dengan NRP : <?php echo $_SESSION['NRP']; ?>)
						</p>
					</div>
				</div>

			</div>
			<!-- akhir dari bungkus -->

		</div>
	</div>
</div>
</div>


<script src=".../assets/js/jquery.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>

<script>

	// Loading Page

	var myVar;



	function myFunction() {

	myVar = setTimeout(showPage, 1000);

	}



	function showPage() {

	document.getElementById("loader").style.display = "none";

	document.getElementById("myDiv").style.display = "block";

	}
</script>
</body>
</html>

<?php } ?>