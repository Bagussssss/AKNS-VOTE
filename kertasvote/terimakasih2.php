<?php 
include '../koneksi.php';
session_start();
if (!isset($_SESSION['NRP'])) {
	?>
	<script type="text/javascript">
		window.location.href="../index.php?k=home";
	</script>
	<?php 
} else {
	
	?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">   

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<title>TERIMAKASIH</title>
		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<script src="../assets/alert/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="../assets/alert/sweetalert2.min.css">
		<style type="text/css">
			.wadah {
				background: white;
				border: 1px solid rgb(218, 220, 224);
				margin: auto;
				margin-top: 50px;
				border-radius: 5px;
				box-shadow: 1px 1px 1px rgba(0,0,0,0.1);
				padding-bottom: 40px;
				padding-top: 15px;
				padding-right: 30px;
				padding-left: 30px;
			}
			.wadah p, i, b{margin-top: 0px; margin-bottom: 0px; font-family: arial; }
			@font-face {
				font-family: "blending-reg";
				/*src : url('../assets/fonts/Blending italic.otf');*/
				src : url('../assets/fonts/GoogleSans-Regular.ttf');
			}
			*{font-family: blending-reg, arial;}
		</style>
	</head>
	<body>
		<?php  
		$nrp = $_SESSION['NRP'];
		$query = mysqli_query($conn, "SELECT * FROM voting inner join pemilih on voting.nrp = pemilih.nrp where voting.NRP = '$nrp'");
		$r = mysqli_fetch_array($query);
		?>

		<div class="container">
			<div class="row">
				<!-- <div class="col-md-"></div> -->
				<div class="col-md-12">
					<div class="wadah">
						<div class="row">
							<div class="col-md-12 text-center">
								<img src="../assets/img/photo.jpg" width="10%">
								<img src="../assets/img/akns.jpg" width="13%">

								<h1>Terima Kasih Atas Partisipasinya</h1>

								<p>Anda Sudah Melakukan Vote Atas Nama <?php echo $r['NAMA']; ?></p>
								<p style="font-style: italic;">Waktu : <?php echo $r['WAKTU']; ?></p>
								<br>
								<a href="outvote.php" class="btn btn-primary btn-sm" style="width: 30%;">Lihat Real-Count</a>

								<br><br>
								<p style="font-size: 10px; color: grey; font-family: arial;">
									<i>*Klik Tombol <b>Lihat Real-Count</b> untuk memonitoring perhitungan suara.</i>
								</p>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>

	</body>
	</html>

	<?php } ?>