<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Berdasarkan Juruasn'],
          ['MMB',     11],
          ['TI',      2]
        ]);

        var options = {
          title: 'Jumlah Voter Berdasarkan Jurusan'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart2'));

        chart.draw(data, options);
      }
    </script>


    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Semua'],
          ['Sudah Vote',     20],
          ['Belum Vote',      10]
        ]);

        var options = {
          title: 'Berdasarkan Responden',
          colors: ['#1abc9c', '#e74c3c']
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>

</head>
<body>

<div class="row wadah">
	<div class="col-lg-12">
		<div class="row">
			<div class="col-lg-12">
				<h2><i class="fas fa-database"></i> Statistik</h2>
				<p>Anda Bisa memonitoring jalannya pemilihan di halaman ini.</p>
				<hr>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-6">
				<div id="piechart" style="width: 100%;"></div>
			</div>
			<div class="col-lg-6">
				<div id="piechart2" style="width: 100%;"></div>
			</div>
		</div>

	</div> <!-- akhir col-lg-12 -->
</div>
</body>
</html>