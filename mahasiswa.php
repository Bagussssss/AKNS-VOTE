<!DOCTYPE html>
<html>
<head>
	<title></title>

	<?php 
		include 'koneksi.php';
		error_reporting(0);
		// menghitung total hak pilih/suara
		$suaratotal = mysqli_query($conn, "SELECT NRP FROM pemilih");
		$totalhakpilih = mysqli_num_rows($suaratotal);
		// query untuk menghitung suara masuk
		$vote = mysqli_query($conn, "SELECT NOMER_URUT FROM voting");
		$totalvote = mysqli_num_rows($vote);

		$presentasesuaramasuk = ($totalvote/$totalhakpilih * 100);

		// $suaratidakmasuk = (100 - $presentasesuaramasuk);

		// echo $suaratidakmasuk;


	?>
</head>
<body>

<div class="row wadah efek">
	<div class="col-lg-12">

		<div class="row">
			<div class="col-lg-12">
				<h2><i class="fas fa-poll-h"></i> Profil Calon</h2>
				<p>Anda Bisa melihat Profil dan mengenal Visi & Misi Calon di halaman ini.</p>
				<hr style="margin-bottom: 5px;">
			</div>
		</div>

		<div class="row" style="margin-top: 10px;">
			<div class="col-md-12" id="wadahnya">
				
				<div class="row">

					<?php 
						// melakukan perulangan berdasarkan banyak calon
						$totalcalon = mysqli_query($conn, "SELECT * FROM capres");
						$i = 1;
						while ($rowcalon = mysqli_fetch_array($totalcalon)) {
							?>

								<div class="col-md-6">
									<div class="thumbnail fotoview">
								      <img src="assets/img/<?php echo $rowcalon['FOTOCALON'] ?>" style="height: 290px;">
								      <div class="caption text-center">
								        <h4 style="margin-bottom: 3px;">PASLON 0<?= $rowcalon['NOMER_URUT'] ?></h4>
								        <p style="margin-top: 0px;"><?= $rowcalon['NAMA_CAPRES'] ?> - <?= $rowcalon['NAMA_CAWAPRES'] ?></p>
								        <p style="font-size: 10px;"><?php echo $rowcalon['SLOGAN']; ?></P>
								        <a href="?k=profilview&nomer_calon=<?php echo $rowcalon['NOMER_URUT']; ?>" class="btn btn-info btn-sm" >View Profile</a>
								      </div>
								    </div>
								</div>	


							<?php

							$i++;

						}
					?>

				</div>

				

			</div> <!-- akhir dari wadah -->
		</div>




		
	</div> <!-- akhir col-lg-12 -->

</div>
</body>
</html>

