<?php 
 include 'koneksi.php';
	$ambil = mysqli_query($conn, "SELECT NRP FROM PEMILIH WHERE JURUSAN = 'TI'");
	$ambil2 = mysqli_query($conn, "SELECT NRP FROM PEMILIH WHERE JURUSAN = 'MM'");
	$ambil3 = mysqli_query($conn, "SELECT NRP FROM PEMILIH WHERE JURUSAN = 'DOSEN'");
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
    <style type="text/css">
		.input {
	     position: relative;
	    }
	    .input input:focus {
	      outline: none;
	    }
	    .input .text {
	      font: 12px/16px 'Helvetica Neue', Arial, sans-serif;
	      position: absolute;
	      top: 50%;
	      left: 15px;
	      margin-top: -8px;
	      color: gray;
	      background-color: inherit;
	      transition: all .1s linear;
	      pointer-events: none;
	    }
	    .input input:focus + .text, .input input:not(:invalid) + .text {
	      color: grey;
	      top: 0%;
	      margin-top: -14px;
	      background: transparent;
	      padding-right: 1px; padding-left: 1px;
	      font-size: 10px;
	      font-weight: 400;
	    }
	    .input textarea:focus + .text, .input textarea:not(:invalid) + .text {
	      color: grey;
	      top: 0%;
	      margin-top: -8px;
	      background: white;
	      padding-right: 1px; padding-left: 1px;
	      font-size: 10px;
	      font-weight: 400;
	    }
	    .input input:focus + .text {
	      color: #2196F3;
	      font-weight: bold;
	    }
	    .input textarea:focus + .text {
	      color: #2196F3;
	      font-weight: bold;
	    }
	</style>
</head>
<body>
<div class="row efek">
	<div class="col-lg-12">
		<div class="row">
			<div class="col-lg-12">
				<div id="myCarousel" class="carousel slide" data-ride="carousel" style="box-shadow: 0 0 1px grey;">
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
						<li data-target="#myCarousel" data-slide-to="1"></li>
						<!-- <li data-target="#myCarousel" data-slide-to="2"></li>		 -->
					</ol>

					<!-- deklarasi carousel -->
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<img src="assets/img/bem51.jpg" alt="" style="width: 100%; max-height: 410px;">
							<div class="carousel-caption">
								<a href="">
									<h3>Berbasis Komputer & Online</h3>
									<p>Pemilihan Presiden BEM AKNS.</p>
								</a>
							</div>
						</div>

						<div class="item">
							<img src="assets/img/vote11.png" alt="" style="width: 100%; max-height: 410px;">
							<div class="carousel-caption">
								<a href="">
									<h3>Berbasis Komputer & Online</h3>
									<p>Pemilihan Presiden BEM AKNS.</p>
								</a>
							</div>
						</div>			
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="colom" style="width: 100%;/* background-color: #eee;*/ background: #f5f5f5; padding: 10px; box-sizing: border-box; padding-right: 20px; padding-left: 20px; box-shadow: 0 0 1px grey;">
					<div class="row">
						<!-- <div class="col-md-1"></div> -->
						<div class="col-md-6">
							<h3 style="margin-top: 0px; margin-bottom: 2px;">Hak Suara</h3>
							<p style="margin-top: 0px; text-align: justify;">Aplikasi ini dibuat agar mempermudah mahasiswa AKNS untuk menggunakan hak suaranya dalam pemilihan presiden BEM yang diadakan setiap tahunnya oleh kampus, setiap mahasiswa hanya dapat mem-VOTE satu kali.</p>
						</div>
						<div class="col-md-2"></div>
						<div class="col-md-4">
							<form method="POST" action="">
								<div class="input">
									<input type="text" name="NRP" class="form-control" style="margin-bottom: 5px; margin-top: 5px;" autocomplete="off" required="">
									<span class="text">Masukkan NRP</span>
								</div>
								<div class="input">
									<input type="text" name="NAMA" class="form-control" required="" style="margin-bottom: 10px; margin-top: 13px;" autocomplete="off">
									<span class="text">Masukkan Nama Lengkap</span>
								</div>
								<button class="btn btn-info btn-sm" name="masuk">Login</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		

		<!-- tempat login kuy -->
		<?php 
		   // session_start();
		   if (isset($_POST['masuk'])) 
		   {
		      $usr = $_POST['NRP'];
		      $pass = $_POST['NAMA'];

		      // query untuk login bos
		      $sql = mysqli_query($conn, "SELECT * FROM PEMILIH where NRP ='$usr' AND NAMA ='$pass'");
		      $data = mysqli_fetch_array($sql);
		      $count = mysqli_num_rows($sql);
		      $nama_user = $data['NRP'];

		      // cek boskuh apakah dia sudah vote atau belum
		      $querycek = mysqli_query($conn, "SELECT NRP FROM voting where NRP = '$usr'");
		      $cekcount = mysqli_num_rows($querycek);

		      $chekopenvote = mysqli_query($conn, "SELECT STATUS FROM up_voting");


		      if (mysqli_num_rows($chekopenvote) == 1) 
		      {
  			      if ($count > 0) 
			      {
			      	if ($cekcount == 0) 
				      	{
				         $_SESSION['NRP'] = $nama_user;
				         ?>
				            <script type="text/javascript">
				               window.location.href="kertasvote/index.php";
				            </script>
				         <?php 
				      	} 
			      	else 
				  	  {
					    $_SESSION['NRP'] = $nama_user;
					    ?>
					    <script>
				  	  		window.location.href='kertasvote/terimakasih2.php';
				  	  	</script>
				  	  	<?php
				  	  }
			  	  }
			      else 
			      {
			        ?>
			        	<script>
			        		Swal.fire({
							  type: 'error',
							  title: 'Oops...',
							  text: 'Pastikan Username dan Password Benar !!!',
							});
			        	</script>

			        <?php
			      }	
		      
		      } else {
		      	?>
		        	<script>
		        		Swal.fire({
						  type: 'error',
						  title: 'Oops...',
						  text: 'Session Untuk Voting Sudah Berakhir',
						});
		        	</script>

			        <?php
		      }



		   }
		?>



		<!-- <div class="row" style="margin-top: 20px;">
			<div class="col-lg-6">
				<div id="piechart" style="width: 100%;"></div>
			</div>
			<div class="col-lg-6">
				<div id="piechart2" style="width: 100%;"></div>
			</div>
		</div> -->

	</div> <!-- akhir dati col-lg-12 wadah -->
</div>
</body>
</html>