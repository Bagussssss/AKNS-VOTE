-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 11, 2019 at 01:17 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `voteakns`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `ID_ADMIN` int(11) NOT NULL,
  `NAMA_PENGGUNA` varchar(50) DEFAULT NULL,
  `USERNAME` varchar(10) DEFAULT NULL,
  `PASSWORD` varchar(9) DEFAULT NULL,
  `EMAIL` varchar(40) DEFAULT NULL,
  `NO_HP` varchar(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`ID_ADMIN`, `NAMA_PENGGUNA`, `USERNAME`, `PASSWORD`, `EMAIL`, `NO_HP`) VALUES
(1, 'admin', 'admin', 'admin', 'admin', 'admin'),
(2, 'Yosi', 'admin', 'admin', 'yosibagusdsd@gmail.com', '082330723223');

-- --------------------------------------------------------

--
-- Table structure for table `capres`
--

CREATE TABLE `capres` (
  `NOMER_URUT` int(11) NOT NULL,
  `NAMA_CAPRES` varchar(50) DEFAULT NULL,
  `PEKERJAAN_CAPRES` varchar(40) DEFAULT NULL,
  `PROFIL_CAPRES` varchar(30) DEFAULT NULL,
  `JURUSAN_CAPRES` varchar(20) NOT NULL,
  `NAMA_CAWAPRES` varchar(50) DEFAULT NULL,
  `PEKERJAAN_CAWAPRES` varchar(40) DEFAULT NULL,
  `PROFIL_CAWAPRES` varchar(30) DEFAULT NULL,
  `JUR_CAWAPRES` varchar(20) NOT NULL,
  `VISI` varchar(500) NOT NULL,
  `MISI` varchar(4000) NOT NULL,
  `SLOGAN` varchar(100) DEFAULT NULL,
  `FOTOCALON` varchar(100) NOT NULL,
  `program_kerja` varchar(40000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `capres`
--

INSERT INTO `capres` (`NOMER_URUT`, `NAMA_CAPRES`, `PEKERJAAN_CAPRES`, `PROFIL_CAPRES`, `JURUSAN_CAPRES`, `NAMA_CAWAPRES`, `PEKERJAAN_CAWAPRES`, `PROFIL_CAWAPRES`, `JUR_CAWAPRES`, `VISI`, `MISI`, `SLOGAN`, `FOTOCALON`, `program_kerja`) VALUES
(1, 'Ilham Maulana Ishak', 'Mahasiswa', 'Sumenep, 22 April 2000', 'Teknik Informatika', 'Zamronih', 'Mahasiswa', 'Sumenep, 35 April 2000', 'Multimedia', 'Mewujudkan Badan Eksekutif USD sebagai pilar inisiator yang berkontribusi aktif dengan bersinergi menjunjung tinggi nilai keberagaman.', '<ol>\r\n	<li>Menciptakan keterbukaan iklim internal BEM yang menyenangkan dengan semangat kebersamaan.</li>\r\n	<li>Mewujudkan organisasi mahasiswa sebagai wadah kreatifitas dan apresiasi terhadap karya mahasiswa.</li>\r\n	<li>Menumbuhkan budaya kritis di kalangan mahasiswa USD dengan berlandaskan perjuangan akan nilai kebenaran terhadap isu nasional &amp; sosial lingkungan.</li>\r\n	<li>BEM USD sebagai garda terdepan dalam isu pergerakan strategis internal dan eskternal yang intelektual ,dinamis, dan responsif terhadap isu-isu sosial internal-eksternal.</li>\r\n	<li>Membentuk budaya organisasi yang inklusif dan profesional.</li>\r\n</ol>\r\n', 'Membangun Bersama', 'kuyk2.jpg', '<ol>\r\n	<li>Menyelesaikan permasalahan atau isu-isu yang ada di Fakultas Teknik dengan jalan adanya, diskusi, kajian dan advokasi</li>\r\n	<li>Kajian terhadap isu-isu atau permasalahan di tulis pada sebuah kertas artikel yang akan ditempel di madding</li>\r\n	<li>Progress kajian kastrad akan ditampilkan dalam bentuk video yang akan di upload di IG dan youtube BEM FT UNEJ</li>\r\n	<li>Parameter keberhasilan yaitu dapat terselesaikan isu-isu tersebut.</li>\r\n</ol>\r\n'),
(2, 'Syafiq Zayyadi R.', 'Mahasiswa', 'Sumenep, 22 April 2000', 'Teknik Informatika', 'Rezky Wulan A.', 'Mahasiswa', 'Sumenep, 35 April 2000', 'Teknik Informatika', 'Menjadi BEM Universitas Potensi Utama sebagai tempat pembinaan untuk melahirkan kader-kader berkualitas dan tempat berkarya bagi seluruh mahasiswa Menjadikan BEM Universitas Potensi Utama lembaga mahasiswa yang edukatif, solitif, dinamis, kritis dan aktualis', '<ol>\r\n	<li>Membentuk struktur BEM Universitas Potensi Utama yang kokoh</li>\r\n	<li>Menciptakan tradisi diskusi, menulis, membaca sehingga kader memiliki pemahaman dan intelektualitas dalam bertindak.</li>\r\n	<li>Menumbuhkan kesolidan, kedisiplinan, toleransi antar sesama mahasiswa Universitas Potensi Utama</li>\r\n	<li>Menjadi wadah untuk menyalurkan minat dan bakat mahasiswa Universitas Potensi Utama</li>\r\n	<li>Menjadi wadah aspirasi dan pembelaan terhadap hak-hak Civitas Akademika</li>\r\n</ol>\r\n', 'Mengarang Indah', 'kuyk.jpg', '<ol>\r\n	<li>Membangun hubungan yang sinergis dengan pihak Institut untuk memenuhi kebutuhan setiap mahasiswa.</li>\r\n	<li>Membangun budaya berdiskusi dan berbahasa guna meningkatkan karakter mahasiswa baik di lingkungan kampus maupun di masyarakat.</li>\r\n	<li>Menjadi wadah untuk memperjuangkan hak-hak mahasiswa dan menyalurkan aspirasi mahasiwa Institut Pertanian Malang</li>\r\n	<li>Bersama membangun sinergis dan mengembangkan potensi minat dan bakat mahasiswa</li>\r\n	<li>Meningkatkan partisipasi aktif mahasiswa IPM dalam mewujudkan manfaat nyata dalam masyarakat.</li>\r\n	<li>Aktif, dan responsif terhadap isu internal maupun eksternal</li>\r\n	<li>Menumbuh kembangkan rasa persaudaraan dan kekeluargaan diantara mahasiswa.</li>\r\n</ol>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `pemilih`
--

CREATE TABLE `pemilih` (
  `NRP` varchar(20) NOT NULL,
  `NAMA` varchar(50) DEFAULT NULL,
  `JENKEL` varchar(5) NOT NULL,
  `JURUSAN` varchar(20) DEFAULT NULL,
  `ALAMAT` varchar(20) DEFAULT NULL,
  `NOTELP` varchar(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemilih`
--

INSERT INTO `pemilih` (`NRP`, `NAMA`, `JENKEL`, `JURUSAN`, `ALAMAT`, `NOTELP`) VALUES
('198320121001', 'Taufikur Rahman', 'L', 'DOSEN', 'SUMENEP', '082xx'),
('198320121002', 'Ahmed David Anugerah', 'L', 'DOSEN', 'SUMENEP', '082xx'),
('198320121003', 'Ahmad Walid Hujari', 'L', 'DOSEN', 'SUMENEP', '082xx'),
('198320121004', 'Deny Ferdiansyah Putra', 'L', 'DOSEN', 'SUMENEP', '082xx'),
('198320121005', 'Lusiana Agustien', 'P', 'DOSEN', 'SUMENEP', '082xx'),
('198320121006', 'Rully Widiastutik', 'P', 'DOSEN', 'SUMENEP', '082xx'),
('198320121007', 'Nur Afni Fatimah', 'P', 'DOSEN', 'SUMENEP', '082xx'),
('198320121008', 'Livika Dwi Jayanti', 'P', 'DOSEN', 'SUMENEP', '082xx'),
('198320121009', 'Miftah Farid', 'L', 'DOSEN', 'SUMENEP', '082xx'),
('198320121010', 'Indrasari Manggaranti', 'P', 'DOSEN', 'SUMENEP', '082xx'),
('198320121011', 'Arda Gusema', 'P', 'DOSEN', 'SUMENEP', '082xx'),
('198320121012', 'Rizal Fasa Nograha', 'L', 'DOSEN', 'SUMENEP', '082xx'),
('198320121013', 'Khairul Hafid', 'L', 'DOSEN', 'SUMENEP', '082xx'),
('198320121014', 'M. Hantok Sudarto', 'L', 'DOSEN', 'SUMENEP', '082xx'),
('198320121015', 'Martina Sixiana Atmaja', 'P', 'DOSEN', 'SUMENEP', '082xx'),
('198320121016', 'Aditya Maulana', 'L', 'DOSEN', 'SUMENEP', '082xx'),
('198320121017', 'Royhanatul Fitriyah', 'P', 'DOSEN', 'SUMENEP', '082xx'),
('198320121018', 'Taufik Bambang Setya', 'L', 'DOSEN', 'SUMENEP', '082xx'),
('198320121019', 'Mery Ika Widiyati', 'P', 'DOSEN', 'SUMENEP', '082xx'),
('198320121020', 'Edi Suwatama', 'L', 'DOSEN', 'SUMENEP', '082xx'),
('198320121021', 'Meidy Santoso', 'L', 'DOSEN', 'SUMENEP', '082xx'),
('198320121022', 'Moh. Riddoi', 'L', 'DOSEN', 'SUMENEP', '082xx'),
('198320121023', 'Herman', 'P', 'DOSEN', 'SUMENEP', '082xx'),
('198320121024', 'Muhammad Nur Ikwan', 'P', 'DOSEN', 'SUMENEP', '082xx'),
('198320121025', 'Nur Lailatul Hikmah', 'L', 'DOSEN', 'SUMENEP', '082xx'),
('2102177005', 'Alvin Hardi Anugerah', 'L', 'TI', 'SUMENEP', '085xx'),
('2102177009', 'Eriko Bani', 'L', 'TI', 'SUMENEP', '082xxx'),
('2102177014', 'Mardiana', 'P', 'TI', 'SUMENEP', '082xx'),
('2102177028', 'Ach. Fahrur Rozzi', 'L', 'TI', 'SUMENEP', '08xxx'),
('2102177031', 'Anggraini Utami', 'P', 'TI', 'SUMENEP', '082xx'),
('2102177033', 'Desti Yuniati', 'P', 'TI', 'SUMENEP', '08xxx'),
('2102177034', 'Dimas Lukman Hakim', 'L', 'TI', 'SUMENEP', '082xx'),
('2102177036', 'Izzah Iedil Adha', 'P', 'TI', 'Aeng Panas', '827'),
('2102177038', 'Muhammad Kholid Naqsyabandi', 'L', 'TI', 'Sumenep', '087xx'),
('2102177039', 'M. Wildan Taufiqur Rofani', 'L', 'TI', 'SUMENEP', '082xx'),
('2102177041', 'Nurfa firda yurisma', 'P', 'TI', 'Kangean', '028382666738'),
('2102177042', 'Nurim Maftuh', 'L', 'TI', 'SUMENEP', '08xx'),
('2102177043', 'Puteri Laili M.K', 'P', 'TI', 'SUMENEP', '0823xx'),
('2102177044', 'Ardiansyah', 'L', 'TI', 'Sumenep', '082-'),
('2102177047', 'Shafiyul Anam', 'L', 'TI', 'Sumenep', '08xx'),
('2102177049', 'Suliman', 'L', 'TI', 'SUMENEP', '082xx'),
('2102177051', 'Veani Nurrida Putri', 'P', 'TI', 'SUMENEP', '082xx'),
('2102177053', 'Yosi Bagus Sadar Rasuli', 'L', 'TI', 'SUMENEP', '082330723223'),
('2102177067', 'Makhshushah', 'P', 'TI', 'Guluk guluk', '082xx'),
('2102177072', 'Moh. Subhan Adi Wicaksono', 'L', 'TI', 'SUMENEP', '08xxx'),
('2102177077', 'Nur aini', 'L', 'TI', 'Dusun batuguluk', '085330287526'),
('2103197121', 'Yosi Bagus Sadar Rasuli', 'L', 'TI', 'Sumenep', '082330723223');

-- --------------------------------------------------------

--
-- Table structure for table `up_voting`
--

CREATE TABLE `up_voting` (
  `ID_UP` int(11) NOT NULL,
  `STATUS` varchar(10) DEFAULT NULL,
  `ID_ADMIN` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `up_voting`
--

INSERT INTO `up_voting` (`ID_UP`, `STATUS`, `ID_ADMIN`) VALUES
(1, '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `voting`
--

CREATE TABLE `voting` (
  `ID` int(11) NOT NULL,
  `NRP` varchar(20) DEFAULT NULL,
  `NOMER_URUT` int(11) DEFAULT NULL,
  `WAKTU` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `voting`
--

INSERT INTO `voting` (`ID`, `NRP`, `NOMER_URUT`, `WAKTU`) VALUES
(32, '198320121001', 2, '20-July-2019, 03:00'),
(33, '198320121009', 2, '20-July-2019, 03:00'),
(34, '198320121020', 2, '20-July-2019, 03:01'),
(35, '198320121019', 1, '20-July-2019, 03:01'),
(36, '2102177028', 2, '20-July-2019, 03:02'),
(37, '2102177009', 1, '20-July-2019, 03:02'),
(38, '2102177005', 2, '20-July-2019, 03:02'),
(39, '2102177031', 1, '20-July-2019, 03:03'),
(40, '2102177044', 1, '20-July-2019, 03:03'),
(41, '2102177043', 2, '20-July-2019, 03:04'),
(42, '2102177042', 2, '20-July-2019, 03:04'),
(43, '2102177036', 2, '21-July-2019, 11:20'),
(44, '2102177053', 1, '21-July-2019, 11:39'),
(45, '198320121002', 2, '21-July-2019, 11:40'),
(46, '198320121003', 1, '21-July-2019, 11:42'),
(47, '2102177014', 2, '21-July-2019, 11:47'),
(48, '2102177033', 1, '11-August-2019, 04:4'),
(49, '198320121005', 2, '10-September-2019, 0'),
(50, '2103197121', 1, '02-December-2019, 11'),
(51, '198320121011', 1, '07-December-2019, 12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`ID_ADMIN`);

--
-- Indexes for table `capres`
--
ALTER TABLE `capres`
  ADD PRIMARY KEY (`NOMER_URUT`);

--
-- Indexes for table `pemilih`
--
ALTER TABLE `pemilih`
  ADD PRIMARY KEY (`NRP`);

--
-- Indexes for table `up_voting`
--
ALTER TABLE `up_voting`
  ADD PRIMARY KEY (`ID_UP`),
  ADD KEY `ID_ADMIN` (`ID_ADMIN`);

--
-- Indexes for table `voting`
--
ALTER TABLE `voting`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_CAPRES_VOTING` (`NOMER_URUT`),
  ADD KEY `FK_PELIMILH_VOTING` (`NRP`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `ID_ADMIN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `voting`
--
ALTER TABLE `voting`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `up_voting`
--
ALTER TABLE `up_voting`
  ADD CONSTRAINT `up_voting_ibfk_1` FOREIGN KEY (`ID_ADMIN`) REFERENCES `admin` (`ID_ADMIN`);

--
-- Constraints for table `voting`
--
ALTER TABLE `voting`
  ADD CONSTRAINT `FK_CAPRES_VOTING` FOREIGN KEY (`NOMER_URUT`) REFERENCES `capres` (`NOMER_URUT`),
  ADD CONSTRAINT `FK_PELIMILH_VOTING` FOREIGN KEY (`NRP`) REFERENCES `pemilih` (`NRP`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
