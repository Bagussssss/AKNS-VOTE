<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<div class="row wadah">
	<div class="col-lg-12">

		<div class="row">
			<div class="col-md-12">
				<h2><i class="fas fa-book"></i> Ketentuan</h2>
				<p>Untuk bisa menggunakan hak suara dalam pemilihan, anda harus mempunyai NRP aktif sebagai mahasiswa AKNS.</p>
				<hr>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-4 text-center">
				<img src="assets/img/login-icon-png-20.png" width="100%" style="max-height: 220px;">
			</div>

			<div class="col-lg-8">
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-12">
								<h4><i class="far fa-user"></i> Mahasiswa</h4>
								<ul>
									<li><i class="far fa-address-card"></i> Login Dengan menggunakan NRP & Nama dari masing-masing Mahasiswa.</li>
									<li><i class="fas fa-vote-yea"></i> Setiap Mahasiswa hanya bisa memilih (Vote) satu kali.</li>
									<li><i class="fas fa-vote-yea"></i> Suara dari Mahasiswa dimonitoring langsung oleh admin.</li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-12">
								<h4><i class="far fa-user"></i> Dosen</h4>
								<ul>
									<li><i class="far fa-address-card"></i> Login Dengan menggunakan Kode Pengajar & Nama dari masing-masing Dosen.</li>
									<li><i class="fas fa-vote-yea"></i> Setiap Dosen hanya bisa memilih (Vote) satu kali.</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12 text-justify">
				<h4>-> Read Me !!!</h4>
				<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ketentuan diatas berlaku bagi para calon pemilih, dosen ataupun mahasiswa hanya mempunyai satu hak pilih sesuai dengan sistem demokrasi pada umumnya (Bebas & Rahasia). <!-- Untuk melihat NRP aktif silahkan <a href="">Klik Disini</a> . --></p>
			</div>
		</div>

	</div><!-- akhir col-lg-12 wadah -->
</div>
</body>
</html>