<!DOCTYPE html>
<html>
<head>
	<title>RUANG VOTING | BEM</title>
	<!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous"> -->
	<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap1.css">
	<link rel="stylesheet" type="text/css" href="../assets/css/input.css" />
	<link rel="stylesheet" type="text/css" href="style.css" />
	<link rel="stylesheet" type="text/css" href="../assets/fontku/css/all.css">
	<style type="text/css">
		@font-face {
			font-family: "google-sans";
			src : url('../assets/fonts/GoogleSans-Regular.ttf');
		}
		*{font-family: google-sans;}
		.header {
			border-radius: 100%;
			width: 120px;
			height: 120px;
			padding-top: 45px;
			background-color: black;
		}
		.menu a:link, a:visited {
			color: #34ac98;
		}
		.menu a{text-decoration: none; font-weight: inherit; font-size: 17px;}
		.menu a i {font-size: 25px; color: #000; padding-right: 10px;}
		.menu a:hover {color: red; margin-left: 5px; transition: 0.4s;}
		.content li {line-height: 50px; background-color: #eff8f7; width: 100px; display: inline-table;}
		.carousel-caption {top :0; margin-top: 220px; text-align: right; left: 65%; width: 35%;}
		.carousel-caption h3 {margin-top: 50px; margin-bottom: 0px; padding: 5px; background-color: white; color: black;}
		.carousel-caption p {margin-top: 0px; padding: 5px; background-color: black;}
		.carousel-caption a:hover {text-decoration: none;}
		.content p {color: #60bebf;}
	</style>
</head>
<body>

<div class="container" style="margin-top: 40px; padding-left: 30px; padding-right: 30px;">
	<div class="row">
		<div class="col-lg-2">
			<div class="row">
				<div class="col-md-12 text-center">
					<div class="header text-center">
						<h4 style="margin-top: 0px; color: white;">RUANG VOTING</h4>
					</div>
				</div>
			</div>
			<aside class="sidebar sidebar-collapse">
			<div class="row menu" style="margin-top: 35px;">
				<div class="col-md-12 text-left">
					<div class="row">
						<div class="col-md-12">
							<a style="line-height: 50px; display: block;" href="">
								<i class="far fa-address-card"></i> Mahasiswa
							</a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<a style="line-height: 50px; display: block;" href="">
								<i class="far fa-address-card"></i> Dosen
							</a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<a style="line-height: 50px; display: block;" href="">
								<i class="fas fa-comment-alt" style=""></i> Forum Diskusi
							</a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<a style="line-height: 50px; display: block;" href="">
								<i class="fas fa-poll-h" style="padding-right: 13px; font-size: 27px;"></i> Quick Count
							</a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<a style="line-height: 50px; font-size: 17px; display: block;" href="">
								<i class="fas fa-book" style="padding-right: 13px; font-size: 27px;"></i> Ketentuan 
							</a>
						</div>
					</div>
				</div>
			</div>

			<div class="row side" style="margin-top: 30px;">
				<div class="col-md-12">
					<h5 style="margin-bottom: 0px;">LATEST TWEET</h5>
					<p style="margin-top: 0px; margin-bottom: 0; color: grey;">- - - - - - - - - - - - - - - -</p>
					<a href="" target="_blank"><i class="fab fa-youtube"></i> Ikuti Kami</a>
				</div>
			</div>
			<div class="row side" style="margin-top: 30px;">
				<div class="col-md-12">
					<h5 style="margin-bottom: 0px;">LET'S BE FRIENDS!</h5>
					<p style="margin-top: 0px; margin-bottom: 0; color: grey;">- - - - - - - - - - - - - - - -</p>
					<a href="" target="_blank"><i class="fab fa-facebook-f"></i> Ikuti Kami</a>
				</div>
			</div>

		</div> <!-- akhir dari col-lg-2 -->


		<div class="col-lg-10">

			<div class="row">
				<div class="col-md-12">
					<div id="myCarousel" class="carousel slide" data-ride="carousel">
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<!-- <li data-target="#myCarousel" data-slide-to="2"></li>		 -->
						</ol>
			 
						<!-- deklarasi carousel -->
						<div class="carousel-inner" role="listbox">
							<div class="item active">
								<img src="../assets/img/18.jpg" alt="" style="width: 100%; height: 420px;">
								<div class="carousel-caption">
									<a href="">
										<h3>Berbasis Komputer & Online</h3>
										<p>Pemilihan Presiden BEM AKNS.</p>
									</a>
								</div>
							</div>
							<div class="item">
								<img src="../assets/img/bem2.jpg" alt="" style="width: 100%; height: 420px;">
								<div class="carousel-caption">
									<h3>Tutorial Bootstrap</h3>
									<p>Belajar tutorial bootstrap dasar di www.malasngoding.com</p>
								</div>
							</div>				
						</div>
					</div>
				</div>  
			</div> <!-- akhir dari row didalam col -->

			<div class="row sidebar sidebar-collapse">
				<div class="col-md-12 content text-center">					
					<div class="content" style="background: #eff8f7; height: 85px;">
					<div class="row">
						<div class="col-md-4">
							<p style="margin-top: 10px;">LINK 3</p>
							<p>Pertama Di Madura.</p>
						</div>
						<div class="col-md-4" style="border-left: 1px solid #d3ede9; border-right: 1px solid #d3ede9; height: 85px;">
							<p style="margin-top: 10px;">LINK 2</p>
							<p>Tata Cara Yang Mudah</p>
						</div>
						<div class="col-md-4">
							<p style="margin-top: 10px;">LINK 4</p>
							<p>BEM AKNS</p>
						</div>
					</div>
					</div>
				</div>
			</div>

   		</div> <!-- akhir dari col-lg-10 -->
	
</aside>		

		</div>
	</div>
</div>

<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>


<script type='text/javascript'>
//<![CDATA[
var Nanobar=function(){var c,d,e,f,g,h,k={width:"100%",height:"2px",zIndex:9999,top:"0"},l={width:0,height:"100%",clear:"both",transition:"height .3s"};c=function(a,b){for(var c in b)a.style[c]=b[c];a.style["float"]="left"};f=function(){var a=this,b=this.width-this.here;0.1>b&&-0.1<b?(g.call(this,this.here),this.moving=!1,100==this.width&&(this.el.style.height=0,setTimeout(function(){a.cont.el.removeChild(a.el)},100))):(g.call(this,this.width-b/4),setTimeout(function(){a.go()},16))};g=function(a){this.width=
a;this.el.style.width=this.width+"%"};h=function(){var a=new d(this);this.bars.unshift(a)};d=function(a){this.el=document.createElement("div");this.el.style.backgroundColor=a.opts.bg;this.here=this.width=0;this.moving=!1;this.cont=a;c(this.el,l);a.el.appendChild(this.el)};d.prototype.go=function(a){a?(this.here=a,this.moving||(this.moving=!0,f.call(this))):this.moving&&f.call(this)};e=function(a){a=this.opts=a||{};var b;a.bg=a.bg||"#2f71aa";this.bars=[];b=this.el=document.createElement("div");c(this.el,
k);a.id&&(b.id=a.id);b.style.position=a.target?"relative":"fixed";a.target?a.target.insertBefore(b,a.target.firstChild):document.getElementsByTagName("body")[0].appendChild(b);h.call(this)};e.prototype.go=function(a){this.bars[0].go(a);100==a&&h.call(this)};return e}();
var nanobar = new Nanobar();nanobar.go(30);nanobar.go(60);nanobar.go(100);
//]]>
</script>

</body>
</html>