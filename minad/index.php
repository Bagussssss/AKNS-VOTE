<?php 
include '../koneksi.php'; 
session_start(); 
if (!isset($_SESSION['ID_ADMIN'])) {
  ?>
    <script>window.location.href='../log/index.php';</script>
  <?php
}
else {
  $id = $_SESSION['ID_ADMIN'];
  $ambil = mysqli_query($conn, "SELECT NAMA_PENGGUNA FROM admin where ID_ADMIN='$id'");
  $row = mysqli_fetch_array($ambil);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Administrator</title>
    <link href="../assets/admin/bootstrap1.css" rel="stylesheet">
	<script src="../assets/alert/sweetalert2.min.js"></script>
	<link rel="stylesheet" href="../assets/alert/sweetalert2.min.css">
	<link rel="stylesheet" type="text/css" href="../assets/plugin/datatables.min.css">
	<link rel="stylesheet" type="text/css" href="../assets/plugin/data/css/dataTables.bootstrap.min.css">
    <link href="../assets/admin/css/admin2.css" rel="stylesheet">
    <link rel="stylesheet" href="../assets/admin/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/fontku/css/all.css">
    <style type="text/css">
      h2 span {font-size: 13px; color: grey;}
      .shadow {
        box-shadow: 10px 10px 10px solid grey;
      }
      @media (min-width: 768px) {
        .side-nav>li>a {
            width: 298px;
        }
      }
      .navbar-brand {
        box-shadow: 3px 1px 1px rgba(0, 0, 0, 0.2);
        float: left;
        height: 70px;
        padding: 15px 15px;
        background-color: white;
        padding-right: 75px;
        font-size: 18px;
        line-height: 36px;
        border-bottom: 1px solid #dadce0;
      }
        .navbar-inverse .navbar-nav>li>a:hover,
        .navbar-inverse .navbar-nav>li>a:focus {
          color: black;
        }

      .navbar-inverse .navbar-brand:hover,
      .navbar-inverse .navbar-brand:focus {
        background-color: white;
        color: black;
      }
      .side-nav>li.dropdown>ul.dropdown-menu>li>a {
        color: white;
        padding: 15px 15px 15px 25px;
      }
      @font-face {
        font-family: "roboto";
        src : url('../assets/fonts/roboto.light.ttf');
      }
      @font-face {
        font-family: "reg-roboto";
        src :url('../assets/fonts/roboto.regular.ttf');
      }
      @font-face {
        font-family: "google-sans";
        src : url('../assets/fonts/GoogleSans-Regular.ttf');
      }
      *{font-family: google-sans;}
      #page1 .link1, #page2 .link2, #page3 .link3, #page4 .link4, #page5 .link5 {
        color: #1a73e6;
        background-color: #dadce0;
      }
      hr { margin-top: 0px; }
    </style>
</head>
<body>
    <div id="wrapper">
      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="border: none;background: #1565c0;">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php"><img src="../assets/img/rev2.jpg" style="width: 200px; margin-left: 10px;"></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav side-nav" style="box-shadow: 3px 1px 1px rgba(0,0,0,0.2); width: 300px; background: white; border: none; margin-top: 20px;">
            <li><a href="?src=home" class="link1" style=" padding-left: 20px; font-size: 15px;"><i class="fa fa-home" style="font-size: 20px; margin-right: 20px;"></i> Dashboard</a></li> 
            <li><a href="?m=pemilih" class="link2" style=" padding-left: 20px; font-size: 15px;"><i class="fa fa-user" style="font-size: 20px; margin-right: 20px;"></i> Data Pemilih</a></li> 
            <li><a href="?m=capres" class="link4" style="padding-left: 20px; font-size: 15px;"><i class="fa fa-briefcase" style="font-size: 15px; margin-right: 20px;"></i> Data Capres</a></li>
            <li><a href="?m=voting" class="link3" style="padding-left: 20px; font-size: 15px;"><i class="fa fa-bar-chart-o" style="font-size: 16px; margin-right: 20px;"></i> Data Voting</a></li>
            <li><a href="?m=operator" class="link5" style="padding-left: 20px; font-size: 15px;"><i class="glyphicon glyphicon-wrench" style="font-size: 16px; margin-right: 20px;"></i> Operator</a></li>
          </ul>

          <ul class="nav navbar-nav navbar-right navbar-user">
            <li class="dropdown user-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color: white; line-height: 37px; text-transform: capitalize;"><i class="fa fa-user"></i> <?php echo $row['NAMA_PENGGUNA']; ?><b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href=""><i class="fa fa-power-off"></i> Setting</a></li>
                <li class="divider"></li>
                <li><a href="logout.php"><i class="fa fa-power-off"></i> Log Out</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </nav>


<!-- bagian isi -->
      <div id="page-wrapper" style="padding: 15px;">
      	<div class="row">
      		<div class="col-md-1" style="margin-left: 35px;"></div>
      		<div class="col-md-10">
      			<?php
		          if (isset($_GET['m'])) {
		          $page = $_GET['m'];
		            switch ($page) {
		              case 'home':
		                include 'home/home.php';
		                break;
		               case 'pemilih':
		                include 'pemilih/pemilih.php';
		                break;
		               case 'capres':
		                include 'capres/capres.php';
		                break;
		               case 'voting':
		                include 'voting/voting.php';
		                break;
		               case 'inputpemilih':
		                include 'pemilih/inputdata.php';
		                break;
                   case 'operator':
                     include 'operator/operator.php';
                     break;
                    case 'inputcapres':
                      include 'capres/inputcapres.php';
                      break;
                    case 'updatecapres':
                      include 'capres/updatecapres.php';
                      break;
                    case 'editpemilih':
                      include 'pemilih/editpemilih.php';
                      break;
                    case 'editoperator':
                      include 'operator/editdata.php';
                      break;
		            }
		          } else {
		              include 'home/home.php';
		            }
		          ?>	
      		</div>
      	</div>
      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->
    <!-- akhir dari bagian isi -->


<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/plugin/data/js/jquery.dataTables.min.js"></script>
<script src="../assets/plugin/data/js/dataTables.bootstrap.min.js"></script>
<script>
	$(".menu li > a").click(function(e){
		$(".menu ul ul").slideUp(), $(this).next().is(":visible") || $(this).next().slideDown(), e.stopPropagation()
	})

	$(window).bind("load resize", function(){
		if ($(this).width() < 768) {
			$(".sidebar-collapse").addClass("collapse");
		}
		else {
			$(".sidebar-collapse").removeClass("collapse");
			$(".sidebar-collapse").removeAttr("style");
		}
	})

	$(document).ready( function () {
    	$('#mytable').DataTable();
      $('#mytable2').DataTable();
} );
</script>
</body>
</html>


<?php } ?>