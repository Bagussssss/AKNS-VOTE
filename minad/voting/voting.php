<?php include '../koneksi.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body id="page3">

<h2><i class="fa fa-bar-chart-o"></i> Data Voting <span>Monitoring</span></h2><hr>


<div class="row" style="margin-bottom: 10px;">
	<div class="col-md-12">
		<a href="voting/laporan.php" target="_blank" class="btn btn-primary btn-xs">Ambil Laporan</a>
		<a href="voting/bersihkan_riwayat.php" onclick="return confirm('Yakin Ingin Menghapus Data ?')" class="btn btn-danger btn-xs">Hapus Riwayat Voting</a>
		<?php $chek = mysqli_query($conn, "SELECT STATUS FROM up_voting"); $hasil = mysqli_num_rows($chek); $input = 1; ?>
		<?php 
			if (mysqli_num_rows($chek) == 1) {
				echo "<a href='voting/hapus.php?id=". $hasil ."' class='btn btn-warning btn-xs'>Close Vote</a>";
			}
			else {
				echo "<a href='voting/tambah.php?id=". $input ."' class='btn btn-success btn-xs' name='buka'>Open Vote</a>";
			}
		?>
		<span style="color: grey; font-size: 10px;">
			 <?php 
			 	if (mysqli_num_rows($chek) == 1) {
			 		echo "Status : Voting Sedang Dibuka";
			 	}
			 	else {
			 		echo "Status : Voting Ditutup";
			 	}
			 ?>
		</span>
	</div>
</div>


<div class="table-responsive">
	<table class="table table-bordered" id="mytable2">
		<thead>
			<tr>
				<th width="10">No.</th>
				<th>Responden</th>
				<th>Waktu Vote</th>
			</tr>	
		</thead>
		
		<tbody>
			<?php 
				$i = 1;
				$ambil = mysqli_query($conn, "SELECT voting.NRP, voting.WAKTU, pemilih.JURUSAN, voting.NOMER_URUT FROM voting JOIN pemilih on voting.nrp = pemilih.nrp order by WAKTU");

				while ($r = mysqli_fetch_array($ambil)) {
					
					echo "<tr>
							<td>". $i."</td>
							<td>". $r['NRP']. " / ". $r['JURUSAN'] ."</td>
							<td>". $r['WAKTU']."</td>
						  </tr>
					";

					$i++;
				}
			?>
		</tbody>
	</table>
</div>

</body>
</html>