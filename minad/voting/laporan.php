<!DOCTYPE html>
<html>
<head>
  <title>.</title>
  <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
</head>
<body>
  <?php 
  include '../../koneksi.php';
  $laki = mysqli_query($conn, "SELECT * from voting inner join pemilih on voting.nrp = pemilih.nrp where JURUSAN = 'TI'"); 
  $countti = mysqli_num_rows($laki); 
  $cewek = mysqli_query($conn, "SELECT * from voting inner join pemilih on voting.nrp = pemilih.nrp where JURUSAN = 'MM'");
  $countmm = mysqli_num_rows($cewek);
  $dosen = mysqli_query($conn, "SELECT * FROM voting inner join pemilih on voting.nrp = pemilih.nrp where JURUSAN = 'DOSEN'");
  $countdosen = mysqli_num_rows($dosen);
  $jumhak = mysqli_query($conn, "SELECT * FROM pemilih"); $jummhs = mysqli_num_rows($jumhak);
  ?>
  <div class="container">
    <div class="row mt-4 py-2 bg-light" id="infoku">
      <div class="col-md">
        <div class="card text-white mb-0 p-0">
          <div class="card-body text-center p-2">
            <h2>LAPORAN VOTING</h2>
            <h3>Pemilihan Presiden BEM AKNS</h3>
            <hr style="margin-top: 0px; margin-bottom: 0px;">
          </div>
        </div>


        <div class="laporan" style="margin-top: 30px;">
          <h4>Laporan Umum :</h4>
          <table class="table-bordered" width="100%" style="text-align: center;">
           <thead>
             <tr style="font-weight: bold;">
               <td width="20%">Pemilihan Tahun</td>
               <td>Jumlah Hak Suara</td>
               <td colspan="4">Pemilih Yang Melakukan Vote</td>
               <td>Tidak Memilih</td>
             </tr>
           </thead>

           <tbody>
             <tr>
               <td rowspan="2"><?php echo gmdate('Y'); ?></td>
               <td rowspan="2"><?= $jummhs; ?></td>
               <td width="">TI</td>
               <td width="">MM</td>
               <td>Dosen</td>
               <td>Total</td>
               <td rowspan="2"><?php echo $jummhs - ($countti+$countmm+$countdosen); ?></td>
             </tr>
             <tr>             
               <td><?= $countti; ?></td>
               <td><?= $countmm; ?></td>
               <td><?= $countdosen; ?></td>
               <td><?= $countti + $countmm + $countdosen; ?></td>
             </tr>
           </tbody>
         </table>
       </div>


       <div class="hasil pt-0 portfolio" style="margin-top: 30px;">
        <h4>Laporan Hasil :</h4>
        <table width="100%">
          <tr>
            <?php 
            error_reporting(0);

            $ti = mysqli_query($conn, "SELECT * from voting inner join pemilih on voting.nrp = pemilih.nrp where JURUSAN = 'TI'");
            $jurusanTI = mysqli_num_rows($ti);

            $MM = mysqli_query($conn, "SELECT * from voting inner join pemilih on voting.nrp = pemilih.nrp where JURUSAN = 'MM'");
            $jurusanMM = mysqli_num_rows($MM);

          //MENGHITUNG BERAPA TOTAL PEMILIH
            $hitung = mysqli_query($conn, "SELECT count(nrp) as no from pemilih");
            $tot = mysqli_fetch_array($hitung);
            $responden = $tot['no'];

          //perulangan berdasarkan berapa banyak calon
            $ambil = mysqli_query($conn, "SELECT * from capres");
            $i=1; 
            while ($data = mysqli_fetch_array($ambil)) {
          //mengambil TOTAL suara berdasarkan nomer urut calon
              $query = mysqli_query($conn, "SELECT count(NOMER_URUT) AS sua From voting WHERE NOMER_URUT = '$i'");
              $result = mysqli_fetch_array($query);
              $id = $result['sua'];

          //MENGHITUNG PRESENTASE
              $voting = ($id / $responden * 100);
          //MENAMPILKAN PRESENTASE DIMULAI DARI INDEX KE-0 (ANGKA PERTAMA) SAMPAI INDEK KE-4
              $totvot = substr($voting, 0, 4);




              $q = mysqli_query($conn, "SELECT count(NOMER_URUT) AS sua From voting");
              $r = mysqli_fetch_array($q);
              $idd = $r['sua'];
              $pemilihvot = ($idd / $responden * 100);
              $pemilinonvot = 100 - $pemilihvot;


              $noah = mysqli_query($conn, "SELECT count(NOMER_URUT) AS sua From voting inner join pemilih on pemilih.nrp = voting.nrp where jurusan = 'TI'");
              $band = mysqli_fetch_array($noah);
              $iddKU = $band['sua'];
              $jurusanTI = ($iddKU / $responden * 100);

              $peter = mysqli_query($conn, "SELECT count(NOMER_URUT) AS sua From voting inner join pemilih on pemilih.nrp = voting.nrp where jurusan = 'MM'");
              $mn = mysqli_fetch_array($peter);
              $iddKUku = $mn['sua'];
              $jurusanMM = ($iddKUku / $responden * 100);          

              ?>
              <td>
                <div class="panel panel-default thum text-center">
                  <div class="panel-body thum">
                    <img src="../../assets/img/<?php echo $data['FOTOCALON']; ?>" width="200">
                  </div>
                  <div class="panel-heading">
                    <span>Paslon <?php echo $data['NOMER_URUT']; ?></span>
                    <h3 class="panel-title"><?php echo $totvot; ?>%</h3>
                    <p class="text-center">Jumlah Suara : <?php echo $id; ?></p> 
                  </div>
                </div>
              </td>


              <?php
              $i++;
            }
            ?>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="container" style="margin-top: 20px;">
  <h4>Presentase Pemilih</h4>
  <h5>Presentase Umum :</h5>
  <table width="100%" class="table-bordered table-striped">
    <tr>
      <td width="20%" style="padding-left: 4px;">Mem-Vote</td>
      <td style="padding: 5px;"><img src="progres.jpg" style="height: 20px; width: <?php echo $pemilihvot; ?>%" >
        <?= substr($pemilihvot, 0, 4); ?>%
      </td>
    </tr>
    <tr>
      <td width="20%" style="padding-left: 4px;">Tidak Mem-Vote</td>
      <td style="padding: 5px;"><img src="progres.jpg" style="height: 20px; width: <?php echo $pemilinonvot; ?>%" >
        <?= substr($pemilinonvot, 0, 4); ?>%</td>
      </tr>
    </table>


    <h5>Presentase Berdasarkan Jurusan :</h5>
    <table width="100%" class="table-bordered table-striped">
      <tr>
        <td width="20%" style="padding-left: 4px;">Partisipasi TI</td>
        <td style="padding: 5px;"><img src="progres.jpg" style="height: 20px; width: <?php echo (int)$jurusanTI; ?>%" >
          <?php echo substr($jurusanTI, 0, 4); ?>%
        </td>
      </tr>
      <tr>
        <td width="20%" style="padding-left: 4px;">Partisipasi MM</td>
        <td style="padding: 5px;"><img src="progres.jpg" style="height: 20px; width: <?php echo (int)$jurusanMM; ?>%" >
         <?php echo substr($jurusanMM, 0, 4); ?>%</td>
       </tr>
     </table>
   </div>
 </div>
 <div style="margin-bottom: 500px;"></div>


 <div class="container">
  <div class="row">
    <div class="col-lg-12">


      <?php
      // menghitung total hak pilih/suara
    $suaratotal = mysqli_query($conn, "SELECT NRP FROM pemilih");
    $totalhakpilih = mysqli_num_rows($suaratotal);
    // query untuk menghitung suara masuk
    $vote = mysqli_query($conn, "SELECT NOMER_URUT FROM voting");
    $totalvote = mysqli_num_rows($vote);

    $presentasesuaramasuk = ($totalvote/$totalhakpilih * 100);

    // $suaratidakmasuk = (100 - $presentasesuaramasuk);

    // echo $suaratidakmasuk;
 
              // melakukan perulangan berdasarkan banyak calon
      $totalcalon = mysqli_query($conn, "SELECT * FROM capres");
      $i = 1;
      while ($rowcalon = mysqli_fetch_array($totalcalon)) {

        $totalsuara = mysqli_query($conn, "SELECT NOMER_URUT FROM voting WHERE NOMER_URUT = '$i'");
        $totalsuaracalon = mysqli_num_rows($totalsuara);

                // menghitung presentase
        $voting = ($totalsuaracalon/$totalhakpilih * 100);
        $presentasevoting = substr($voting, 0, 4); 


                //query yang di pakai pada table statistik untuk akumulasi
        $TI = mysqli_query($conn, "SELECT pemilih.NRP, voting.NOMER_URUT FROM pemilih join voting on pemilih.NRP = voting.NRP where voting.NOMER_URUT='$i' AND pemilih.JURUSAN='TI'");
        $DOS = mysqli_query($conn, "SELECT pemilih.NRP, voting.NOMER_URUT FROM pemilih join voting on pemilih.NRP = voting.NRP where voting.NOMER_URUT='$i' AND pemilih.JURUSAN='DOSEN'");
        $MM = mysqli_query($conn, "SELECT pemilih.NRP, voting.NOMER_URUT FROM pemilih join voting on pemilih.NRP = voting.NRP where voting.NOMER_URUT='$i' AND pemilih.JURUSAN='MM'");

        $akumulasiTI = mysqli_num_rows($TI) / $totalsuaracalon * $presentasevoting;
        $akumulasiDOSEN =  mysqli_num_rows($DOS) / $totalsuaracalon * $presentasevoting;
        $akumulasiMM =  mysqli_num_rows($MM) / $totalsuaracalon * $presentasevoting;
        ?>
<br>
        <h2>Presentase Nomer Urut <?php echo $rowcalon['NOMER_URUT']; ?></h2>
        <div class="row thumbnail" style="margin-left: 1px; width: 99.9%; margin-bottom: 500px;">
          <div class="col-md-12">
            <div class="col-md-5" style="margin-top: 15px;">
              <img src="../../assets/img/<?= $rowcalon['FOTOCALON'] ?>" style="width: 100%">
            </div>

            <div class="col-md-7" style="margin-top: 5px;">
              <div class="row">
                <div class="col-md-12">
                  <div>
                    <h4>PASLON 0<?= $rowcalon['NOMER_URUT'] ?></h4>
                    <p style="margin-top: 0px;"><?= $rowcalon['NAMA_CAPRES']  ?> - <?= $rowcalon['NAMA_CAWAPRES']  ?></p>
                    <div class="">
                      <img src="progres.jpg" style="height: 20px; width: <?php echo $presentasevoting ?>%" >
                      <?php echo $presentasevoting; ?>%
                    </div>
                  </div>
                </div>
              </div>



              <div class="row" id="table-statistik">
                <div class="col-md-12">
                  <span>Statistik Pemilih</span>
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <tr>
                        <th>Teknik Informatika</th>
                        <th>Multimedia</th>
                        <th>Dosen</th>
                        <th>Total</th>
                      </tr>
                      <tr>
                        <td><?php echo substr($akumulasiTI, 0,4); ?>%</td>
                        <td><?php echo substr($akumulasiMM, 0,4); ?>%</td>
                        <td><?php echo substr($akumulasiDOSEN, 0,4); ?>%</td>
                        <td><?php echo $akumulasiTI + $akumulasiDOSEN + $akumulasiMM; ?>%</td>
                      </tr>

                      <tr>
                        <td><?php echo $T = mysqli_num_rows($TI); ?> Orang</td>
                        <td><?php echo $M = mysqli_num_rows($MM); ?> Orang</td>
                        <td><?php echo $D = mysqli_num_rows($DOS); ?> Orang</td>
                        <td><?php echo $T + $M + $D; ?> Orang</td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>  



        <?php

        $i++;

      }


      ?>


    </div>
  </div>


</div> <!-- akhir dari wadah -->

<script type="text/javascript">
  window.print();
  // window.location.href="../../index.php?m=voting";
  // history.go(-1);
</script>
</body>
</html>