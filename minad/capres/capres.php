<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body id="page4">

	<h2><i class="fa fa-briefcase"></i> Data Capres <span>Data Master</span></h2><hr>

	<div class="row" style="margin-bottom: 10px;">
		<div class="col-md-12">
			<a href="?m=inputcapres" class="btn btn-primary btn-xs">Tambah Capres</a>
		</div>
	</div>

	<div class="table-responsive">
		<table class="table table-bordered">
			<tr>
				<th>No Urut</th>
				<th>Nama Capres</th>
				<th>Nama Cawapres</th>
				<th>Foto Resmi</th>
				<th colspan="2">Action</th>
			</tr>

			<?php 
			$sql = mysqli_query($conn, "SELECT * FROM CAPRES order by NOMER_URUT");
			while ($data = mysqli_fetch_array($sql)) { ?>
				<tr>
					<td><?= $data['NOMER_URUT']; ?></td>
					<td><?= $data['NAMA_CAPRES']; ?></td>
					<td><?= $data['NAMA_CAWAPRES'] ?></td>
					<td><img width="100" src="../assets/img/<?= $data['FOTOCALON']; ?>"></td>
					<td>
						<form method="post" action="">
							<input type="hidden" name="id" value="<?php echo $data['NOMER_URUT']; ?>">
							<button class="btn btn-primary btn-xs" name="view">View</button>
						</form>
					</td>
					<td>
						<a href="?m=updatecapres&id=<?= $data['NOMER_URUT'] ?>" class="btn btn-primary btn-xs" name="update">Edit</a>
						<a href="capres/hapus_data.php?id=<?php echo $data['NOMER_URUT'] ?>" class="btn btn-danger btn-xs tombol-hapus">Delete</a>
					</td>
				</tr>
				<?php
			}
			?>
		</table>
	</div>



	<div class="row">

		<div class="col-md-12">
			<?php 
			if (isset($_POST['view'])) {

				$id = $_POST['id'];

				$query = mysqli_query($conn, "SELECT * FROM capres where NOMER_URUT = '$id'");
				$d = mysqli_fetch_array($query); ?>
				<div class="row thumbnail" style="padding-top: 20px; margin-left: 1px; width: 99.9%; background: whitesmoke;">
					<div class="col-md-6">
						<!-- <h3 style="margin-top: 0px; text-align: center;">Profil Calon Presiden & Wakil Presiden Nomer Urut</h3> -->

						<center><b>Visi</b>
							<p><?php echo $d['VISI']; ?></p>
							<b>Misi</b></center>
							<p><?php echo $d['MISI']; ?></p>
							<hr>
						</div>

						<div class="col-md-6">
							<div class="thumbnail">
								<img src="../assets/img/<?php echo $d['FOTOCALON'] ?>" style="height: 300px;">
								<div class="caption text-center">
									<h4 style="margin-bottom: 3px;">PASLON 0<?= $d['NOMER_URUT'] ?></h4>
									<p style="margin-top: 0px;"><?= $d['NAMA_CAPRES'] ?> - <?= $d['NAMA_CAWAPRES'] ?></p>
									<p style="font-size: 10px;">"<?php echo $d['SLOGAN']; ?>"</p>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<p>Profil Capres :</p>
							<table class="table">
								<tr>
									<td>Nama Lengkap</td>
									<td>:</td>
									<td><?php echo $d['NAMA_CAPRES']; ?></td>
								</tr>
								<tr>
									<td>Tanggal Lahir</td>
									<td>:</td>
									<td><?php echo $d['PROFIL_CAPRES']; ?></td>
								</tr>
								<tr>
									<td>Pekerjaan</td>
									<td>:</td>
									<td><?php echo $d['PEKERJAAN_CAPRES']; ?></td>
								</tr>
								<tr>
									<td>Jurusan</td>
									<td>:</td>
									<td><?php echo $d['JURUSAN_CAPRES']; ?></td>
								</tr>
							</table>
						</div>
						<div class="col-md-6">
							<p>Profil Cawapres :</p>
							<table class="table">
								<tr>
									<td>Nama Lengkap</td>
									<td>:</td>
									<td><?php echo $d['NAMA_CAWAPRES']; ?></td>
								</tr>
								<tr>
									<td>Tanggal Lahir</td>
									<td>:</td>
									<td><?php echo $d['PROFIL_CAWAPRES']; ?></td>
								</tr>
								<tr>
									<td>Pekerjaan</td>
									<td>:</td>
									<td><?php echo $d['PEKERJAAN_CAWAPRES']; ?></td>
								</tr>
								<tr>
									<td>Jurusan</td>
									<td>:</td>
									<td><?php echo $d['JUR_CAWAPRES']; ?></td>
								</tr>
							</table>
						</div>
					</div>

					<?php
				}
				?>
			</div>

		</div>
<script src="../assets/js/jquery.min.js"></script>
<script type="text/javascript">
	$('.tombol-hapus').on('click', function(e){

		e.preventDefault();
		const href = $(this).attr('href');

		Swal.fire({
		  title: 'apakah anda yakin',
		  text: "Data Capres akan dihapus",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#5cb85c',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Hapus Data!'
		}).then((result) => {
		  if (result.value) {
		    document.location.href = href;
		  }
		})

	});
</script>


	</body>
	</html>