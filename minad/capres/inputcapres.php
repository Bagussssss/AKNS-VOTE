<?php 
	$ambilnomer = mysqli_query($conn, "SELECT NOMER_URUT FROM capres");
	$nomerurut = mysqli_num_rows($ambilnomer);
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
<script type="text/javascript" src="../assets/ckeditor/jquery.js"></script>
<script type="text/javascript" src="../assets/ckeditor/ckeditor.js"></script>
</head>
<body id="page4">

<h2>Tambah Data Capres</h2><hr>

<form method="POST" action="" enctype="multipart/form-data">

	<table>
		<tr>
			<td>
				<a href="?m=capres" class="btn btn-danger btn-xs">Kembali</a>
				<button class="btn btn-success btn-xs" name="simpan">Simpan Data</button>
			</td>
		</tr>
	</table>
	<br>

	<div class="table-responsive">
		<table class="table table-bordered">
			<tr>
				<td style="width: 30%">Nomer Urut</td>
				<td><input type="text" name="NOMER_URUT" class="form-control" placeholder="NOMER URUT" required value="<?php echo $nomerurut+1; ?>" readonly></td>
			</tr>
			<tr>
				<td>NAMA CAPRES</td>
				<td><input type="text" name="NAMA_CAPRES" class="form-control" placeholder="NAMA CAPRES" required></td>
			</tr>
			<tr>
				<td>PEKERJAAN CAPRES</td>
				<td><input type="text" name="PEKERJAAN_CAPRES" class="form-control" placeholder="PEKERJAAN CAPRES" required></td>
			</tr>
			<tr>
				<td>PROFIL CAPRES</td>
				<td><input type="text" name="PROFIL_CAPRES" class="form-control" placeholder="PROFIL CAPRES" required></td>
			</tr>
			<tr>
				<td>JURUSAN CAPRES</td>
				<td>
					<select class="form-control" name="JURUSAN_CAPRES">
						<option value="Teknik Informatika">Teknik Informatika</option>
						<option value="Multimedia">Multimedia</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>NAMA CAWAPRES</td>
				<td><input type="text" name="NAMA_CAWAPRES" class="form-control" placeholder="NAMA CAWAPRES" required></td>
			</tr>
			<tr>
				<td>PEKERJAAN CAWAPRES</td>
				<td><input type="text" name="PEKERJAAN_CAWAPRES" class="form-control" placeholder="PEKERJAAN CAWAPRES" required></td>
			</tr>
			<tr>
				<td>PROFIL CAWAPRES</td>
				<td><input type="text" name="PROFIL_CAWAPRES" class="form-control" placeholder="PROFIL CAWAPRES" required></td>
			</tr>
			<tr>
				<td>JURUSAN CAWAPRES</td>
				<td>
					<select class="form-control" name="JUR_CAWAPRES">
						<option value="Teknik Informatika">Teknik Informatika</option>
						<option value="Multimedia">Multimedia</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>VISI</td>
				<td><input type="text" name="VISI" class="form-control" placeholder="VISI" required></td>
			</tr>
			<tr>
				<td>MISI</td>
				<td><textarea name="MISI" id="MISI"></textarea></td>
			</tr>
			<TR>
				<td>SLOGAN</td>
				<td><input type="text" name="SLOGAN" class="form-control" placeholder="SLOGAN" required=""></td>
			</TR>
			<tr>
				<td>FOTO RESMI CALON</td>
				<td><input type="file" name="FOTO"></td>
			</tr>
			<tr>
				<td>PROGRAM KERJA</td>
				<td><textarea name="program" id="program"></textarea></td>
			</tr>
		</table>
	</div>
</form>
	
</table>


<script>
    $(function () {
        CKEDITOR.replace('MISI');
        CKEDITOR.replace('program');
    });
</script>


<?php 
include '../koneksi.php';
if (isset($_POST['simpan'])) {
	$NOMER_URUT = $_POST['NOMER_URUT'];
	$NAMA_CAPRES = $_POST['NAMA_CAPRES'];
	$PEKERJAAN_CAPRES = $_POST['PEKERJAAN_CAPRES'];
	$PROFIL_CAPRES = $_POST['PROFIL_CAPRES'];
	$JURUSAN_CAPRES = $_POST['JURUSAN_CAPRES'];
	$NAMA_CAWAPRES = $_POST['NAMA_CAWAPRES'];
	$PEKERJAAN_CAWAPRES = $_POST['PEKERJAAN_CAWAPRES'];
	$PROFIL_CAWAPRES = $_POST['PROFIL_CAWAPRES'];
	$JUR_CAWAPRES = $_POST['JUR_CAWAPRES'];
	$SLOGAN = $_POST['SLOGAN'];
	$VISI = $_POST['VISI'];
	$MISI = $_POST['MISI'];
	$nama_file= $_FILES['FOTO']['name'];
  	$source=$_FILES['FOTO']['tmp_name'];
  	$folder = '../assets/img/';
	$PROGRAM = $_POST['program'];


	$SIMPAN = mysqli_query($conn, "INSERT INTO capres values ('$NOMER_URUT', '$NAMA_CAPRES', '$PEKERJAAN_CAPRES', '$PROFIL_CAPRES', '$JURUSAN_CAPRES', '$NAMA_CAWAPRES', '$PEKERJAAN_CAWAPRES', '$PROFIL_CAWAPRES', '$JUR_CAWAPRES', '$VISI', '$MISI', '$SLOGAN', '$nama_file', '$PROGRAM')");
	$upload = move_uploaded_file($source, $folder.$nama_file);
	if ($SIMPAN && $upload) {
			?>

			<script type="text/javascript">
				Swal.fire({
				  type: 'success',
				  title: 'Berhasil',
				  text: 'Data Berhasil Ditambahkan',
				})
			</script>

			<?php
		}
		else { ?>
				
				<script type="text/javascript">
					Swal.fire({
					  type: 'error',
					  title: 'NRP Sudah Ada',
					  text: 'Silahkan Cek Kembali Data Inputan',
					})
				</script>

			<?php
		}

}

?>
</body>
</html>