<?php 
	include '../koneksi.php';

		$id = $_GET['id'];
		$query = mysqli_query($conn, "SELECT * FROM capres where NOMER_URUT ='$id'");
		$r = mysqli_fetch_array($query);
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script type="text/javascript" src="../assets/ckeditor/jquery.js"></script>
<script type="text/javascript" src="../assets/ckeditor/ckeditor.js"></script>
</head>
<body>

<h2>Update Data Capres <span>Data Master</span></h2><hr>


<form method="post" action="" enctype="multipart/form-data">
	
	<table>
		<tr>
			<td>
				<a href="?m=capres" class="btn btn-danger btn-xs">Kembali</a>
				<button class="btn btn-success btn-xs" name="update">Update Data</button>
			</td>
		</tr>
	</table>
	<br>


	<div class="table-responsive">
		<table class="table table-bordered">
			<tr>
				<td style="width: 30%">Nomer Urut</td>
				<td><input type="text" name="NOMER_URUT" class="form-control" placeholder="NOMER URUT" required value="<?php echo $r['NOMER_URUT'] ?>" readonly></td>
			</tr>
			<tr>
				<td>NAMA CAPRES</td>
				<td><input type="text" value="<?php echo $r['NAMA_CAPRES'] ?>" name="NAMA_CAPRES" class="form-control" placeholder="NAMA CAPRES" required></td>
			</tr>
			<tr>
				<td>PEKERJAAN CAPRES</td>
				<td><input type="text" name="PEKERJAAN_CAPRES" value="<?php echo $r['PEKERJAAN_CAPRES'] ?>" class="form-control" placeholder="PEKERJAAN CAPRES" required></td>
			</tr>
			<tr>
				<td>PROFIL CAPRES</td>
				<td><input type="text" name="PROFIL_CAPRES" class="form-control" value="<?php echo $r['PROFIL_CAPRES'] ?>" placeholder="PROFIL CAPRES" required></td>
			</tr>
			<tr>
				<td>JURUSAN CAPRES</td>
				<td>
					<select class="form-control" name="JURUSAN_CAPRES">
						<?php 
							$chek = $r['JURUSAN_CAPRES'];
							if ($chek == "Teknik Informatika") { ?>
									<option value="Teknik Informatika" selected="">Teknik Informatika</option>
									<option value="Multimedia">Multimedia</option>
								<?php
							} else {
								?>
									<option value="Teknik Informatika">Teknik Informasi</option>
									<option value="Multimedia" selected="">Multimedia</option>
								<?php
							}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td>NAMA CAWAPRES</td>
				<td><input type="text" name="NAMA_CAWAPRES" value="<?php echo $r['NAMA_CAWAPRES'] ?>" class="form-control" placeholder="NAMA CAWAPRES" required></td>
			</tr>
			<tr>
				<td>PEKERJAAN CAWAPRES</td>
				<td><input type="text" name="PEKERJAAN_CAWAPRES" value="<?php echo $r['PEKERJAAN_CAWAPRES'] ?>" class="form-control" placeholder="PEKERJAAN CAWAPRES" required></td>
			</tr>
			<tr>
				<td>PROFIL CAWAPRES</td>
				<td><input type="text" name="PROFIL_CAWAPRES" value="<?php echo $r['PROFIL_CAWAPRES'] ?>" class="form-control" placeholder="PROFIL CAWAPRES" required></td>
			</tr>
			<tr>
				<td>JURUSAN CAWAPRES</td>
				<td>
					<select class="form-control" name="JUR_CAWAPRES">
						<?php 
							$chek = $r['JUR_CAWAPRES'];
							if ($chek == "Teknik Informatika") { ?>
									<option value="Teknik Informatika" selected="">Teknik Informatika</option>
									<option value="Multimedia">Multimedia</option>
								<?php
							} else {
								?>
									<option value="Teknik Informatika">Teknik Informasi</option>
									<option value="Multimedia" selected="">Multimedia</option>
								<?php
							}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td>VISI</td>
				<td><input type="text" name="VISI" value="<?php echo $r['VISI'] ?>" class="form-control" placeholder="VISI" required></td>
			</tr>
			<tr>
				<td>MISI</td>
				<td><textarea name="MISI" id="MISIupdate"><?php echo $r['MISI']; ?></textarea></td>
			</tr>
			<TR>
				<td>SLOGAN</td>
				<td><input type="text" name="SLOGAN" value="<?php echo $r['SLOGAN']; ?>" class="form-control" placeholder="SLOGAN" required=""></td>
			</TR>
			<tr>
				<td>FOTO RESMI CALON</td>
				<td>
					<div class="row">
						<div class="col-md-6">
							<input type="file" name="FOTO">
						</div>
						<div class="col-md-6">
							<a href="../assets/img/<?php echo $r['FOTOCALON'] ?>">
							<img width="50" src="../assets/img/<?php echo $r['FOTOCALON'] ?>"></a>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td>PROGRAM KERJA</td>
				<td><textarea name="PROGRAMKERJA" id="PROGRAM"><?php echo $r['program_kerja']; ?></textarea></td>
			</tr>
		</table>
	</div>
</form>
	
</table>
</form>


<script>
    $(function () {
        CKEDITOR.replace('MISIupdate');
    });
</script>
<script>
    $(function () {
        CKEDITOR.replace('PROGRAM');
    });
</script>


<?php 
include '../koneksi.php';
if (isset($_POST['update'])) {
	$NOMER_URUT = $_POST['NOMER_URUT'];
	$NAMA_CAPRES = $_POST['NAMA_CAPRES'];
	$PEKERJAAN_CAPRES = $_POST['PEKERJAAN_CAPRES'];
	$PROFIL_CAPRES = $_POST['PROFIL_CAPRES'];
	$JURUSAN_CAPRES = $_POST['JURUSAN_CAPRES'];
	$NAMA_CAWAPRES = $_POST['NAMA_CAWAPRES'];
	$PEKERJAAN_CAWAPRES = $_POST['PEKERJAAN_CAWAPRES'];
	$PROFIL_CAWAPRES = $_POST['PROFIL_CAWAPRES'];
	$JUR_CAWAPRES = $_POST['JUR_CAWAPRES'];
	$SLOGAN = $_POST['SLOGAN'];
	$VISI = $_POST['VISI'];
	$MISI = $_POST['MISI'];
	$nama_file= $_FILES['FOTO']['name'];
  	$source=$_FILES['FOTO']['tmp_name'];
  	$folder = '../assets/img/';
	$PROGRAM = $_POST['PROGRAMKERJA'];


	if (empty($nama_file)) {
		$SIMPAN = mysqli_query($conn, "UPDATE capres SET NAMA_CAPRES = '$NAMA_CAPRES', PEKERJAAN_CAPRES='$PEKERJAAN_CAPRES', PROFIL_CAPRES='$PROFIL_CAPRES', JURUSAN_CAPRES='$JURUSAN_CAPRES', NAMA_CAWAPRES='$NAMA_CAWAPRES', PEKERJAAN_CAWAPRES='$PEKERJAAN_CAWAPRES', PROFIL_CAWAPRES='$PROFIL_CAWAPRES', JUR_CAWAPRES='$JUR_CAWAPRES', SLOGAN='$SLOGAN', VISI='$VISI', MISI='$MISI', program_kerja = '$PROGRAM' where NOMER_URUT='$NOMER_URUT'");
		if ($SIMPAN) {
				?>

				<script type="text/javascript">
					Swal.fire({
					  type: 'success',
					  title: 'Berhasil',
					  text: 'Data Berhasil Diupdate',
					})
				</script>

				<?php
			}
	}
	else {
		$SIMPAN = mysqli_query($conn, "UPDATE capres SET NAMA_CAPRES = '$NAMA_CAPRES', PEKERJAAN_CAPRES='$PEKERJAAN_CAPRES', PROFIL_CAPRES='$PROFIL_CAPRES', JURUSAN_CAPRES='$JURUSAN_CAPRES', NAMA_CAWAPRES='$NAMA_CAWAPRES', PEKERJAAN_CAWAPRES='$PEKERJAAN_CAWAPRES', PROFIL_CAWAPRES='$PROFIL_CAWAPRES', JUR_CAWAPRES='$JUR_CAWAPRES', SLOGAN='$SLOGAN', VISI='$VISI', MISI='$MISI', FOTOCALON='$nama_file', program_kerja = '$PROGRAM' where NOMER_URUT='$NOMER_URUT'");
		$upload = move_uploaded_file($source, $folder.$nama_file);
		if ($SIMPAN && $upload) {
				?>

				<script type="text/javascript">
					Swal.fire({
					  type: 'success',
					  title: 'Berhasil',
					  text: 'Data Berhasil Diupdate',
					})
				</script>

				<?php
			}
			else { ?>
					
					<script type="text/javascript">
						Swal.fire({
						  type: 'error',
						  title: 'Update Gagal',
						  text: 'Silahkan Cek Kembali Data Inputan',
						})
					</script>

				<?php
			}
	}

}

?>




</body>
</html>