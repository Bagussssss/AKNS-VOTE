<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body id="page2">

<h2>Tambah Data Pemilih</h2><hr>


<form method="post" action="">
	<table>
		<tr>
			<td>
				<a href="?m=pemilih" class="btn btn-danger btn-xs">Kembali</a>
				<button class="btn btn-success btn-xs" name="simpan">Simpan Data</button>
			</td>
		</tr>
	</table>
	<br>
	<table class="table">
		<tr>
			<td width="10">NRP/NIP</td>
			<td><input type="text" name="NRP" class="form-control" placeholder="NRP/NIP" maxlength="15" required></td>
		</tr>
		<tr>
			<td>NAMA</td>
			<td><input type="text" name="NAMA" class="form-control" placeholder="NAMA" required=""></td>
		</tr>
		<tr>
			<td>JENKEL</td>
			<td>
				<input type="radio" name="JENKEL" id="JENKELL" value="L" required=""> 
				<label for="JENKELL">Laki-Laki</label>
				<input type="radio" name="JENKEL" id="JENKELP" value="P" required="">
				<label for="JENKELP">Perempuan</label>
			</td>
		</tr>
		<tr>
			<td>JURUSAN</td>
			<td>
				<select class="form-control" name="JURUSAN" required="">
					<option value="TI">TEKNIK INFORMATIKA</option>
					<option value="MM">MULTIMEDIA BROADCASTING</option>
					<option class="DOSEN">DOSEN</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>ALAMAT</td>
			<td><input type="text" name="ALAMAT" class="form-control" placeholder="ALAMAT" required=""></td>
		</tr>
		<tr>
			<td>NOTELP</td>
			<td><input type="text" name="NOTELP" class="form-control" placeholder="NOTELP" maxlength="12" required=""></td>
		</tr>
	</table>
</form>


<?php 
include '../koneksi.php';
if (isset($_POST['simpan'])) {
	$NRP = $_POST['NRP'];
	$NAMA = $_POST['NAMA'];
	$JENKEL = $_POST['JENKEL'];
	$JURUSAN = $_POST['JURUSAN'];
	$ALAMAT = $_POST['ALAMAT'];
	$NOTELP = $_POST['NOTELP'];

	$simpanpemilih = mysqli_query($conn, "INSERT INTO pemilih values ('$NRP', '$NAMA', '$JENKEL', '$JURUSAN', '$ALAMAT', '$NOTELP')");

	if ($simpanpemilih) { ?>

		<script type="text/javascript">
			Swal.fire({
			  type: 'success',
			  title: 'Berhasil',
			  text: 'Data Berhasil Ditambahkan',
			})
		</script>

		<?php
	}
	else { ?>
			
			<script type="text/javascript">
				Swal.fire({
				  type: 'error',
				  title: 'NRP Sudah Ada',
				  text: 'Silahkan Cek Kembali Data Inputan',
				})
			</script>

		<?php
	}

}

?>


</body>
</html>