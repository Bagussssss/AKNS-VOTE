<?php 
$id = $_GET['id'];

$query = mysqli_query($conn, "SELECT * FROM pemilih where NRP = '$id'");
$r = mysqli_fetch_array($query);

?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body id="page2">

	<h2><i class="fa fa-user"></i> Update Pemilih <span>Data Master</span></h2><hr>

	<form method="post" action="">
		<table>
			<tr>
				<td>
					<a href="?m=pemilih" class="btn btn-danger btn-xs">Kembali</a>
					<button class="btn btn-success btn-xs" name="update">Update Data</button>
				</td>
			</tr>
		</table>
		<br>
		<table class="table">
			<tr>
				<td width="10">NRP/NIP</td>
				<td><input type="text" name="NRP" class="form-control" placeholder="NRP/NIP" maxlength="10" required readonly="" value="<?php echo $r['NRP'] ?>"></td>
			</tr>
			<tr>
				<td>NAMA</td>
				<td><input type="text" name="NAMA" class="form-control" placeholder="NAMA" required="" value="<?php echo $r['NAMA'] ?>"></td>
			</tr>
			<tr>
				<td>JENKEL</td>
				<td>
					<?php 
						if ($r['JENKEL'] == 'L') {
							?>
								<input type="radio" name="JENKEL" id="JENKELL" value="L" required="" checked=""> 
								<label for="JENKELL">Laki-Laki</label>
								<input type="radio" name="JENKEL" id="JENKELP" value="P" required="">
								<label for="JENKELP">Perempuan</label>
							<?php
						} else {
							?>
								<input type="radio" name="JENKEL" id="JENKELL" value="L" required=""> 
								<label for="JENKELL">Laki-Laki</label>
								<input type="radio" name="JENKEL" id="JENKELP" value="P" required="" checked="">
								<label for="JENKELP">Perempuan</label>
							<?php
						}
					?>
				</td>
			</tr>
			<tr>
				<td>JURUSAN</td>
				<td>
					<select class="form-control" name="JURUSAN" required="">
						<?php 
							if ($r['JURUSAN'] == 'TI') {
								?>
								<option value="TI" selected="">TEKNIK INFORMATIKA</option>
								<option value="MM">MULTIMEDIA BROADCASTING</option>
								<option class="DOSEN">DOSEN</option>
								<?php
							} else if ($r['JURUSAN'] == 'MM') {
								?>
								<option value="TI">TEKNIK INFORMATIKA</option>
								<option value="MM" selected="">MULTIMEDIA BROADCASTING</option>
								<option class="DOSEN">DOSEN</option>
								<?php
							} else {
								?>
								<option value="TI">TEKNIK INFORMATIKA</option>
								<option value="MM">MULTIMEDIA BROADCASTING</option>
								<option class="DOSEN" selected="">DOSEN</option>
								<?php
							}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td>ALAMAT</td>
				<td><input type="text" name="ALAMAT" class="form-control" placeholder="ALAMAT" required="" value="<?php echo $r['ALAMAT'] ?>"></td>
			</tr>
			<tr>
				<td>NOTELP</td>
				<td><input type="text" name="NOTELP" value="<?php echo $r['NOTELP'] ?>" class="form-control" placeholder="NOTELP" maxlength="12" required=""></td>
			</tr>
		</table>
	</form>



<?php 
include '../koneksi.php';
if (isset($_POST['update'])) {
	$NRP = $_POST['NRP'];
	$NAMA = $_POST['NAMA'];
	$JENKEL = $_POST['JENKEL'];
	$JURUSAN = $_POST['JURUSAN'];
	$ALAMAT = $_POST['ALAMAT'];
	$NOTELP = $_POST['NOTELP'];

	$update = mysqli_query($conn, "UPDATE pemilih SET NAMA='$NAMA', JENKEL = '$JENKEL', JURUSAN = '$JURUSAN', ALAMAT='$ALAMAT', NOTELP='$NOTELP' WHERE NRP='$NRP'");

	if ($update) { ?>

		<script type="text/javascript">
			Swal.fire({
			  type: 'success',
			  title: 'Berhasil',
			  text: 'Data Berhasil Update',
			})
		</script>

		<?php
	}
	else { ?>
			
			<script type="text/javascript">
				Swal.fire({
				  type: 'error',
				  title: 'Gagal Di Update',
				  text: 'Silahkan Cek Kembali Data Inputan',
				})
			</script>

		<?php
	}

}

?>

</body>
</html>