<?php 
include '../koneksi.php';
$query = mysqli_query($conn, "SELECT * FROM pemilih order by nrp");
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		.table-responsive img {color: grey;}
	</style>
</head>
<body id="page2">

	<h2> <i class="fa fa-user"></i> Data Pemilih <span>Data Master</span></h2><hr>

	<form id="upload_csv" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
				<table class="table table-bordered">
					<tr>
						<td>Tambah Data</td>
						<td colspan="2">Import Data</td>
					</tr>
					<tr>
						<td style="width: 200px;"><a href="?m=inputpemilih" class="btn btn-primary btn-xs" style="width: 100%;">Input Data</a></td>
						<td style="width: 80px;">
							<div class="row">
								<div class="col-sm-6">
									<input type="file" name="employee_file">
								</div>
								<div class="col-sm-6">
									<button name="upload" id="upload" style="width: 100%; margin-top: 5px;" class="btn btn-success btn-xs">Import Data</button>
								</div>
							</div>
						</td>
					</tr>
				</table>
				</div>
			</div>
		</div>
	</form>

	<div class="table-responsive" id="employee_table" style="margin-top: 0px;">
		<table class="table table-bordered table-hover" id="mytable">
			<thead>
				<tr>
					<th width="10">No</th>
					<th>NRP</th>
					<th>Nama</th>
					<th>Jenkel</th>
					<th>Sebagai</th>
					<th>Alamat</th>
					<th>Action</th>
				</tr>
			</thead>

			<tbody>
				<?php 
					$i = 1;
					while ($row = mysqli_fetch_array($query)) { $nrpku = $row['NRP']; ?>

						<tr>
							<td><?= $i++; ?></td>
							<td><?= $row['NRP'] ?></td>
							<td>
								<?= $row['NAMA'] ?>
								<?php 
									$voting = mysqli_query($conn, "SELECT NRP FROM VOTING where NRP = '$nrpku'");
									$chekvote = mysqli_num_rows($voting);

									if ($chekvote == 1) {
										echo "<img src='../assets/img/icon2.svg' width='10'>";
									}
								?>		
							</td>
							<td><?= $row['JENKEL'] ?></td>
							<td>
								<?php 
									if ($row['JURUSAN'] == "TI") {
										echo "MHS TI";
									}
									elseif ($row['JURUSAN'] == "MM") {
										echo "MHS MM";
									}
									else {
										echo $row['JURUSAN'];
									}
								?>
							</td>
							<td><?= $row['ALAMAT'] ?></td>
							<!-- <TD><?= $row['NOTELP'] ?></TD> -->
							<td>
								<a href="pemilih/hapusdata.php?NRP=<?php echo $row['NRP'] ?>" class="btn btn-danger btn-xs tombol-hapus"><i class="glyphicon glyphicon-trash"></i></a>
								<a href="?m=editpemilih&id=<?php echo $row['NRP'] ?>" class="btn btn-warning btn-xs"><i class="glyphicon glyphicon-edit"></i></a>
							</td>
						</tr>

						<?php
					}
				?>
			</tbody>
		</table>
	</div>
<script src="../assets/js/jquery.min.js"></script>
<!-- <script src="../assets/js/bootstrap.min.js"></script> -->

<script>
	$(document).ready(function(){
		$('#upload_csv').on('submit', function(e){
			e.preventDefault();
			$.ajax({
				url : "pemilih/import.php",
				method : "POST",
				data:new FormData(this),
				contentType:false,
				cache:false,
				processData:false,
				success:function(data){
					if (data == 'eror1') {
						Swal.fire({
						  type: 'error',
						  title: 'Oops...',
						  text: 'File Tidak Mendukung, File Harus Format : CSV',
						})
					}
					else if (data == "eror2") {
						Swal.fire({
						  type: 'warning',
						  title: 'Silahkan Select File',
						  text: 'File Harus Format : CSV',
						})
					}
					else {
						$('#employee_table').html(data);
						Swal.fire(
						  'Berhasil',
						  'Data Berhasil Di Export',
						  'success'
						)
					}
				}
			});
		});
	});	

</script>

<script type="text/javascript">
	$('.tombol-hapus').on('click', function(e){

		e.preventDefault();
		const href = $(this).attr('href');

		Swal.fire({
		  title: 'apakah anda yakin',
		  text: "Data pemilih akan dihapus",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#5cb85c',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Hapus Data!'
		}).then((result) => {
		  if (result.value) {
		    document.location.href = href;
		  }
		})

	});
</script>
<!-- 
<script>
    $("#mytable").dataTable();
</script>
 -->

</body>
</html>