<?php 
	$ambilpemilih = mysqli_query($conn, "SELECT NRP FROM pemilih");
	$ambilcapres = mysqli_query($conn, "SELECT NOMER_URUT FROM capres");
	$ambilvote = mysqli_query($conn, "SELECT ID FROM voting");

	//total hak suara berdasarkan jurusan
	$pemilihdosen = mysqli_query($conn, "SELECT NRP FROM pemilih where JURUSAN='DOSEN'");
	$pemilihTI = mysqli_query($conn, "SELECT NRP FROM pemilih where JURUSAN='TI'");
	$pemilihMM = mysqli_query($conn, "SELECT NRP FROM pemilih where JURUSAN='MM'");

	//total hak suara berdasarkan jurusan yang sudah melakukan voting

	$voterdosen = mysqli_query($conn, "SELECT pemilih.NRP, voting.NRP FROM pemilih join voting on pemilih.NRP = voting.NRP where JURUSAN='DOSEN'");
	$voterti = mysqli_query($conn, "SELECT pemilih.NRP, voting.NRP FROM pemilih join voting on pemilih.NRP = voting.NRP where JURUSAN='TI'");
	$votermm = mysqli_query($conn, "SELECT pemilih.NRP, voting.NRP FROM pemilih join voting on pemilih.NRP = voting.NRP where JURUSAN='MM'");


?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		.kotak i {font-size: 80px; color: white;}
		.isi span {font-size: 30px;}
		.isi div {margin-bottom: 10px;}
		.kotak {margin-bottom: 10px;}
	</style>
</head>
<body id="page1">
<h2><i class="fa fa-home"></i> Dashboard <span>Info Utama</span></h2><hr>


<div class="row">
	<div class="col-md-12">

		<div class="alert alert-info alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  <strong>Selamat Datang <?= $row['NAMA_PENGGUNA'] ?>,</strong> Anda Berada Pada Halaman Administrator, Silahkan Monitoring Jalannya Pemilihan dengan <a href="">Klik Disini</a>.
		</div>
		
		<div class="row rowwadahkotak">
			<div class="col-md-4">
				<div class="kotak" style="box-shadow: 0 1px 2px grey;">
					
					<div class="row">
						<div class="col-md-6 text-center" >
							<div style="background-color: #30a6ff; padding: 5px;">
								<i class="fa fa-tasks"></i>
							</div>
						</div>
						<div class="col-md-6 isi">
							<div></div>
							<span><?php echo mysqli_num_rows($ambilpemilih); ?></span>
							<p>Hak Pilih</p>
						</div>
					</div>
				
				</div>
			</div>

			<div class="col-md-4">
				<div class="kotak" style="box-shadow: 0 1px 2px grey;">
					
					<div class="row">
						<div class="col-md-6 text-center" >
							<div style="background-color: #1cc0af; padding: 5px;">
								<i class="far fa-address-card"></i>
							</div>
						</div>
						<div class="col-md-6 isi">
							<div></div>
							<span><?php echo mysqli_num_rows($ambilcapres); ?></span>
							<p>Calon Presiden</p>
						</div>
					</div>
				
				</div>
			</div>

			<div class="col-md-4">
				<div class="kotak" style="box-shadow: 0 1px 2px grey;">
					
					<div class="row">
						<div class="col-md-6 text-center" >
							<div style="background-color: #f9253f; padding: 5px;">
								<i class="fa fa-bar-chart-o"></i>
							</div>
						</div>
						<div class="col-md-6 isi">
							<div></div>
							<span><?php echo mysqli_num_rows($ambilvote); ?></span>
							<p>Responden</p>
						</div>
					</div>
				
				</div>
			</div>
		</div> <!-- akhir row wadah kotak -->


		<div class="row" style="margin-top: 10px;">
			<div class="col-md-6">
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th colspan="2">Alokasi Hak Suara</th>
							</tr>
						</thead>

						<tbody>
							<tr>
								<td>Dosen</td>
								<td><?php echo $d = mysqli_num_rows($pemilihdosen); ?> Suara</td>
							</tr>
							<tr>
								<td>Mahasiswa TI</td>
								<td><?php echo $t = mysqli_num_rows($pemilihTI); ?> Suara</td>
							</tr>
							<tr>
								<td>Mahasiswa MM</td>
								<td><?php echo $m = mysqli_num_rows($pemilihMM); ?> Suara</td>
							</tr>
							<tr>
								<th>Total</th>
								<th><?php echo ($d + $t + $m); ?> Suara</th>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<div class="col-md-6">
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th colspan="2">Yang Melakukan Vote</th>
							</tr>
						</thead>

						<tbody>
							<tr>
								<td>Dosen</td>
								<td><?php echo $vd = mysqli_num_rows($voterdosen); ?> Responden</td>
							</tr>
							<tr>
								<td>Mahasiswa TI</td>
								<td><?php echo $vt = mysqli_num_rows($voterti); ?> Responden</td>
							</tr>
							<tr>
								<td>Mahasiswa MM</td>
								<td><?php echo $vm = mysqli_num_rows($votermm); ?> Responden</td>
							</tr>
							<tr>
								<th>Total</th>
								<th><?php echo ($vt + $vm + $vd); ?> Responden</th>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

		</div>

	</div>
</div>


</body>
</html>