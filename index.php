<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<link rel="shourcut icon" href="assets/img/photo.jpg">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>RUANG VOTING | BEM AKNS</title>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/fontku/css/all.css">
	<link href="assets/nprogress/nprogress.css" rel="stylesheet">
	<link href="assets/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
	<script src="assets/alert/sweetalert2.min.js"></script>
	<link rel="stylesheet" href="assets/alert/sweetalert2.min.css">
<style type="text/css">

	@font-face {
		font-family: "blending-italic";
		src : url('assets/fonts/GoogleSans-Regular.ttf');
		/* src : url('assets/fonts/Blending italic.otf'); */
	}
	@font-face {
		font-family: "blending-reg";
		src : url('assets/fonts/GoogleSans-Regular.ttf');
	}
	@font-face {
		font-family: "polama";
		src : url('assets/fonts/La Paloma.ttf');
	}
  h2 span{
    font-size: 15px;
    color: grey;
  }
  hr {
    margin-top: 0px;
  }
	.wrapper{
		width: 100%;
		height: 100%;
	}
	.navbar {
		margin-bottom: 0px;
	}
	.sidebar {
		width: 100%;
		height: 99%;
		position: relative;
		z-index: 100;
	}
	.sidebar ul {
		padding: 0px;
		margin: -2px 0px 0px;
	}
	.sidebar ul li {
		list-style-type: none;
		border-bottom: 1px solid rgba(255,255,255,.05)
	}
	.sidebar ul li a {
		 color: #34ac98;
		text-decoration: none;
		display: block;
		line-height: 50px;
		transform: all 200ms ease-in;
	}
	.sidebar ul li a:hover {
		text-decoration: none;
		color: red;
	}
	.sedebar ul li a span {
		display: inline-block;
	}
	.sidebar ul ul {
		display: none;
	}
	@media (min-width: 768px){
		.sidebar {
			width: 240px;
		}
		.content {
			margin-left: 250px;
		}
		.inner {
			padding: 15px;
		}
	}
	.menu a{text-decoration: none; font-weight: inherit; font-size: 17px;}
	.menu a i {font-size: 25px; color: #000; padding-right: 10px;}
	.menu li a:hover > i {color: rgba(0,0,0,0.5); margin-left: 5px; transition: 0.4s;}
	.menu a:link, a:visited {color: #34ac98;}
	.carousel-caption {top :0; margin-top: 220px; text-align: right; left: 65%; width: 35%; font-family: blending-reg;}
	.carousel-caption h3 {margin-top: 50px; margin-bottom: 0px; padding: 5px; background-color: white; color: black;}
	.carousel-caption p {margin-top: 0px; padding: 5px; background-color: black;}
	.carousel-caption a:hover {text-decoration: none;}
	.judul {width: 110px; height: 110px; border-radius: 100%; font-family: polama, blending-italic; box-shadow: 1px 2px 2px rgba(0,0,0,0.5);}
	.judul p {margin-top: 25px; font-size: 13px;}
	.menu-content {font-family: blending-italic;}
	.side {font-family: blending-reg;}
	.wadah p {margin-top: 0px; font-family: georgia; font-style: italic;}
	.wadah h2 {margin-bottom: 2px; font-family: blending-reg;}
	.wadah hr {margin-top: 0px; border-top: 2px solid #e5e5e5;}
	.wadah ul li {font-family: georgia; font-style: italic;}
	.footer i {font-size: 25px; font-weight: bold; color: grey;}
	.footer ul li {display: inline-table;}
	.footer a:hover > .wa {color: green;}
	.footer a:hover > .fb {color: #3c5a9a;}
	.footer a:hover > .yt {color: rgb(243, 0, 7);}
	.footer .info a:hover {color: red; text-decoration: none; transition: 0.4s;}
	.berita a:hover {color: red; text-decoration: none; transition: 0.4s;}
	.efek {
        opacity: 0;
        transition: 2s;
        transform: translate(0,0);
      }
      .efek.jalan {
        opacity: 1;
        transform: translate(0,0);
      }
      .garis {display: none;}
      .judulhide {display: none;}
      .sidekuh {margin-top: 20px;}
      @media screen and (max-width: 750px){
      	.isiku {display: none;}
      	/*.judul {display: none;}*/
      	.menu ul li {display: inline-table; margin-left: 3px; margin-right: 3px;}
      	.menu ul li a { font-size: 13px;}
      	.menu ul li a i{ font-size: 13px;}
      	.menu ul li.hiden {display: none;}
      	.menu ul {text-align: center;}
      	.garis {display: block;}
      	.navbar a.judul {margin-top: 0px;}
      	.judul {display: none;}
      	.judulhide {display: block;}
      	.judulhide img {width: 100px;}
      	.judulhide, .judulhide:hover{text-decoration: none; color:}
      	.judulhide:hover {color: red; transition: 0.4s; margin-top: 2px;}
      	.sidekuh {margin-top: 0px;}
      	.fotoview img {max-height: 200px;}
      	.fotocalon img {max-height: 140px;}
      	.nomerurut h4 {text-align: center;}
      }
</style>
</head>
<body>


<div class="container" style="margin-top: 30px; padding-left: 30px; padding-right: 30px; margin-bottom: 10px;">
<div class="wrapper">
	<div class="row">
		<div class="col-md-2 menunya">
			<div class="row">
				<div class="col-md-12">
					<nav class="navbar navbar-default" style="background: white; border: none;">
					    <div class="container-fluid">
						<div class="navbar-header text-center">
						<center>
					      <a href="?k=home" class="navbar-brand text-center judul" style="background: url('assets/img/black.jpg'); color: white; margin-left: -5px;"><p>Akns Vote</p></a>
					      <a href="?k=home" class="judulhide">
					      		<img src="assets/img/hide.png">
					      		<p style="margin-bottom: 0px; margin-top: 10px;"><i class="fa fa-home" style="color: black;"></i> Dashboard </p>
					      </a>
					  	</center>
					     <!--  <a href="?k=home" class="navbar-brand text-center">
					      	<img src="assets/img/bembem.jpg" width="70">
					      </a> -->
					      <!-- <img src="assets/img/akns.jpg" width="100" class="thumbnail" style="border-radius: 100%; box-shadow: 1px 1px 1px grey;"> -->
					    </div>
				      </div>
					</nav>
				</div> <!-- akhir dari col-md-12 -->
			</div> 

			<div class="row sidekuh">
				<div class="col-md-12">
					<aside class="sidebar">
						<div class="menu">
							<ul class="menu-content">
								<li class="hiden">
									<a href="?k=home">
										<i class="fa fa-home"></i> Dashboard
									</a>
								</li>
								<li>
									<a href="?k=mahasiswa" style="cursor: pointer;">
										<i class="glyphicon glyphicon-briefcase"></i> Profil Calon
									</a>
								</li>
								<li>
									<a href="?k=batang">
										<i class="fas fa-poll-h" style=""></i> Real Count
									</a>
								</li>
								<li>
									<a href="?k=ketentuan">
										<i class="fas fa-book"></i> Ketentuan 
									</a>
								</li>
							</ul>
						</div>
						<hr class="garis">
					</aside>
				</div>
			</div> <!-- akhir dari row menu ul li -->

		<div class="isiku">
			<div class="row side" style="margin-top: 40px;">
				<div class="col-md-12">
					<h5 style="margin-bottom: 0px;">SUBSCRIBE</h5>
					<p style="margin-top: 0px; margin-bottom: 0; color: grey;">- - - - - - - - - - - - - - - -</p>
					<a href="https://www.youtube.com/channel/UC6yjhjOcVRIKrHX-QabIKWA" target="_blank" class="btn btn-default btn-xs" style="width: 75%;"><i class="fab fa-youtube"></i> Subscribe Now !!</a>
				</div>
			</div>

			<div class="row side" style="margin-top: 30px; margin-bottom: 10px;">
				<div class="col-md-12">
					<h5 style="margin-bottom: 0px;">LET'S BE FRIENDS!</h5>
					<p style="margin-top: 0px; margin-bottom: 0; color: grey;">- - - - - - - - - - - - - - - -</p>
					<p style="font-family: Georgia; font-size: 10px; font-style: italic;">
						For weekly finds, community recommendations, and more, like us on Facebook.
					</p>
					<a href="https://www.facebook.com/bemmah.akns" target="_blank" class="btn btn-default btn-xs" style="width: 85%;"><i class="fab fa-facebook-f"></i> Join The Community</a>
				</div>
			</div>
		</div>


		</div>	<!-- akhir dari col-md-2 -->

		<!-- nanti ada slidesow disini -->
		<div class="col-md-10">
			<div class="row">
				<div class="col-md-12 badan">
					<?php
					    if (isset($_GET['k'])) {
					    $k = $_GET['k'];
					      switch ($k) {
					        case 'mahasiswa':
					          include 'mahasiswa.php';
					          break;
					        case 'home':
					        	include 'home.php';
					        	break;
					        case 'quickcount':
					        	include 'quick.php';
					        	break;
					        case 'ketentuan':
					        	include 'ketentuan.php';
					        	break;
					        case 'statistik':
					        	include 'statistik.php';
					        	break;
					        case 'batang':
					        	include 'default.php';
					        	break;
					        case 'profilview':
					        	include 'view_profil.php';
					        	break;
					      }
					    } else {
					        include 'home.php';
					      }
					  ?>
				</div>
			</div>
		</div>
	</div> <!-- akhir dari row wadah paling atas -->

	<div class="row footer" style="margin-top: 50px;">
		<div class="col-md-12">

			<div class="row">
				<div class="col-md-12">
					<hr style="border-top: 2px solid #e5e5e5;">
				</div>
			</div>

			<div class="row info">
				<div class="col-xs-6 text-left" style="font-family: georfia; font-size: 12px;">
					<a href="">AKNS</a> |  <a href="">PENS</a> | <a href="">CONTACT</a>
					<p style="font-family: georgia; color: grey; font-size: 11px;">© 2019 akns-vote, by : yosi bagus</p>
				</div>

				<div class="col-xs-6 text-right">
					<ul>
						<li>
							<a href=""><i class="fab fa-whatsapp wa" style="margin-right: 10px;"></i></a>
						</li>
						<li>
							<a href=""><i class="fab fa-facebook-f fb" style="margin-right: 10px;"></i></a>
						</li>
						<li>
							<a href=""><i class="fab fa-youtube yt"></i></a>
						</li>
					</ul>
				</div>	

			</div>

		</div> <!-- akhir dari col-md-12 wadah footer -->
	</div>

</div>
</div>


<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/nprogress/nprogress.js"></script>
<script src="assets/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<script src="assets/build/js/custom.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(window).bind("load resize", function(){
		if ($(this).width() < 768) {
			$(".sidebar-collapse").addClass("collapse");
		}
		else {
			$(".sidebar-collapse").removeClass("collapse");
			$(".sidebar-collapse").removeAttr("style");
		}
		})
	});


	$(window).on('load', function(){
      $('.efek').addClass('jalan');
    });
</script>
</body>
</html>