<!DOCTYPE html>
<html>
<head>
	<title></title>

	<?php 
		include 'koneksi.php';
		error_reporting(0);
		// menghitung total hak pilih/suara
		$suaratotal = mysqli_query($conn, "SELECT NRP FROM pemilih");
		$totalhakpilih = mysqli_num_rows($suaratotal);
		// query untuk menghitung suara masuk
		$vote = mysqli_query($conn, "SELECT NOMER_URUT FROM voting");
		$totalvote = mysqli_num_rows($vote);

		$presentasesuaramasuk = ($totalvote/$totalhakpilih * 100);

		// $suaratidakmasuk = (100 - $presentasesuaramasuk);
		// echo $suaratidakmasuk;


	?>
</head>
<meta http-equiv="refresh" content="900"/>
<body>

<div class="row wadah efek">
	<div class="col-lg-12">

		<div class="row">
			<div class="col-lg-12">
				<h2><i class="fas fa-poll-h"></i> Real Count</h2>
				<p>Anda Bisa melihat secara <b>Real-Count</b> Perolehan Suara dihalaman ini.</p>
				<hr style="margin-bottom: 5px;">
			</div>
		</div>


		<div class="row" style="margin-top: 10px;">
			<div class="col-md-12" id="wadahnya">
				
				<div class="row thumbnail" style="margin-left: 1px; width: 99.9%;">
					<div class="col-lg-12">
						<div class="row">
							<div class="col-lg-12">
								<div class="">
						          <p>Presentase Suara Masuk</p>
						          <div class="">
						            <div class="progress progress_sm" style="margin-bottom: 10px;">
						              <div class="progress-bar" role="progressbar" data-transitiongoal="<?= substr($presentasesuaramasuk, 0,4); ?>" style="background: red;"><?php echo substr($presentasesuaramasuk, 0, 4); ?>%</div>
						            </div>
						          </div>
						          <p style="font-size: 10px; font-weight: bold; margin-top: 0px; color: grey;">
						          	Jumlah Suara Masuk : <?php echo $totalvote; ?> ||
						          	Jumlah Suara Belum Masuk : <?php echo $totalhakpilih - $totalvote; ?> ||
						          	Total Hak Suara : <?php echo $totalhakpilih; ?>
						          </p>
						        </div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-12">


						<?php 
							// melakukan perulangan berdasarkan banyak calon
							$totalcalon = mysqli_query($conn, "SELECT * FROM capres");
							$i = 1;
							while ($rowcalon = mysqli_fetch_array($totalcalon)) {
								
								$totalsuara = mysqli_query($conn, "SELECT NOMER_URUT FROM voting WHERE NOMER_URUT = '$i'");
								$totalsuaracalon = mysqli_num_rows($totalsuara);

								// menghitung presentase
								$voting = ($totalsuaracalon/$totalhakpilih * 100);
								$presentasevoting = substr($voting, 0, 4); 


								//query yang di pakai pada table statistik untuk akumulasi
								$TI = mysqli_query($conn, "SELECT pemilih.NRP, voting.NOMER_URUT FROM pemilih join voting on pemilih.NRP = voting.NRP where voting.NOMER_URUT='$i' AND pemilih.JURUSAN='TI'");
								$DOS = mysqli_query($conn, "SELECT pemilih.NRP, voting.NOMER_URUT FROM pemilih join voting on pemilih.NRP = voting.NRP where voting.NOMER_URUT='$i' AND pemilih.JURUSAN='DOSEN'");
								$MM = mysqli_query($conn, "SELECT pemilih.NRP, voting.NOMER_URUT FROM pemilih join voting on pemilih.NRP = voting.NRP where voting.NOMER_URUT='$i' AND pemilih.JURUSAN='MM'");

								$akumulasiTI = mysqli_num_rows($TI) / $totalsuaracalon * $presentasevoting;
								$akumulasiDOSEN =  mysqli_num_rows($DOS) / $totalsuaracalon * $presentasevoting;
								$akumulasiMM =  mysqli_num_rows($MM) / $totalsuaracalon * $presentasevoting;
						?>


								<div class="row thumbnail" style="margin-left: 1px; width: 99.9%;">
									<div class="col-md-12">
										
											<div class="col-md-5 fotocalon" style="margin-top: 15px;">
												<img src="assets/img/<?= $rowcalon['FOTOCALON'] ?>" style="height: 230px; width: 100%">
											</div>

												<div class="col-md-7" style="margin-top: 5px;">
													<div class="row">
														<div class="col-md-12 nomerurut">
															<div>
													          <h4>PASLON 0<?= $rowcalon['NOMER_URUT'] ?></h4>
													          <p style="margin-top: 0px;"><?= $rowcalon['NAMA_CAPRES']  ?> - <?= $rowcalon['NAMA_CAWAPRES']  ?></p>
													          <div class="">
													            <div class="progress progress_sm">
													              <div class="progress-bar" role="progressbar" data-transitiongoal="<?php echo $presentasevoting ?>"><?php echo $presentasevoting; ?>%</div>
													            </div>
													          </div>
													        </div>
														</div>
													</div>

													

													<div class="row" id="table-statistik">
														<div class="col-md-12">
															<span>Statistik Pemilih</span>
															<div class="table-responsive">
																<table class="table table-bordered">
																	<tr>
																		<th>Teknik Informatika</th>
																		<th>Multimedia</th>
																		<th>Dosen</th>
																		<th>Total</th>
																	</tr>
																	<?php 
																		$jumakumulasiTI = substr($akumulasiTI, 0,4);
																		$jumakumulasiMM = substr($akumulasiMM, 0,4);
																		$jumakumulasiDOS = substr($akumulasiDOSEN, 0,4);
																		$totalakumulasi = $akumulasiTI + $akumulasiDOSEN + $akumulasiMM;
																	?>
																	<tr>
																		<td>
																			<?php 
																				if ($jumakumulasiTI == "NAN") {
																					echo "0";
																				}
																				else {
																					echo $jumakumulasiTI;
																				}
																			?>%
																		</td>
																		<td>
																			<?php 
																				if ($jumakumulasiMM == "NAN") {
																					echo "0";
																				}
																				else {
																					echo $jumakumulasiMM;
																				}
																			?>%
																		</td>
																		<td>
																			<?php
																			 	if ($jumakumulasiDOS == "NAN") {
																					echo "0";
																				}
																				else {
																					echo $jumakumulasiDOS;
																				}  
																			?>%
																		</td>
																		<td>
																			<?php 
																				echo $totalakumulasi;
																			?>%
																		</td>
																	</tr>

																	<tr>
																		<td><?php echo $T = mysqli_num_rows($TI); ?> Orang</td>
																		<td><?php echo $M = mysqli_num_rows($MM); ?> Orang</td>
																		<td><?php echo $D = mysqli_num_rows($DOS); ?> Orang</td>
																		<td><?php echo $T + $M + $D; ?> Orang</td>
																	</tr>
																</table>
															</div>
														</div>
													</div>

												</div>
										</div>
								</div>	



								<?php

								$i++;

							}


						?>


					</div>
				</div>


			</div> <!-- akhir dari wadah -->
		</div>




		
	</div> <!-- akhir col-lg-12 -->

</div>
</body>
</html>

